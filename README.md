# Service Name : docitt_required-condition

- The purpose of required condition service is to add default/custom conditions.
- Used to retrive, accepts, skipped, rejects, delete and submit documents.

## Getting Started

- Require environment setup.
- Repository and MongoDB access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands. 
- Helpful links to start work :
	* https://docs.microsoft.com/en-us/dotnet/core/
	* https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
### Installing

 - Clone source using below git command or source tree

	    git clone <repository_path>
	And go to respective branch for ex develop

		git fetch && git checkout develop	

 - Open command prompt and goto project directory.

 - Restore nuget packages by command

 		dotnet restore -s <Nuget Package Source> <solution_file>

		Ex. dotnet restore -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc Docitt.RequiredCondition.2017.sln 

 - Build solution by command : 

		dotnet build --no-restore <solution_file>

		Ex. dotnet build --no-restore Docitt.RequiredCondition.2017.sln
		
 - Run Solution by command
 
		dotnet run --no-build --project <project path>|
		eg. dotnet run --no-build --project src\Docitt.RequiredCondition.Api\Docitt.RequiredCondition.Api.csproj
		
## Running the tests

- We have created shellscript file(cover.sh) for running tests and code coverage, which are shared on lendfoundry documents.
- To start the test
	* open cmd 
	* goto project directory
	* type command cover.sh and enter
- you can see all tests results and code coverate report with covered number of lines and percentage on cmd.
- Once testing completed successfully from above command code coverage report html file is geneated on path :
 artifacts\coverage\report\coverage_report\index.html 

### What these tests test and why

- We are using xUnit testing and implementing for below projects.
	* Api
	* Client
	* Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  required-condition .
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/required-condition
	* Sample configurations :
	```
	{
    "eventMappings": [
        {
            "EventName": "DocumentAdded",
            "EntityId": "{Data.EntityId}",
            "EntityType": "{Data.EntityType}"
        }
    ],
    "MaxFileSizeInMB": "5",
    "MaxFileUploadCount": "12",
    "ExplanationTemplateName": "RequiredConditionExplanation",
    "ExplanationTemplateVersion": "1.1",
    "ExplanationDocumentName": "{RequestType.RequestId}_Explanation.pdf",
    "PermissibleFileTypes": "pdf,jpg,png,gif,doc,docx,xls,xlsx",
    "AcceptedOrRejectedNote": "Condition {RequestType.Title} {StatusLabel}. {SourceBy}-{CompletedDate.Time}",
    "AddConditionNotificationNote": "Condition {RequestType.Title} has been added for application # {EntityId}",
    "decisionEngineDefaultConditionRuleName": "adddefault_condition",
    "defaultUserInformation": {
        "UserName": "admin@docitt.com",
        "UserRole": "Admin",
        "Issuer": "docitt"
    },
    "RequestOrder": [
        {
            "Name": "Qualifying",
            "OrderNo": 1,
            "SubCategory": [
                {
                    "Name": "Tax Return",
                    "OrderNo": 1
                },
                {
                    "Name": "W-2 & Pay Stubs",
                    "OrderNo": 2
                },
                {
                    "Name": "Assets",
                    "OrderNo": 3
                },
                {
                    "Name": "AssetsLargeDeposits",
                    "OrderNo": 4
                },
                {
                    "Name": "Income",
                    "OrderNo": 5
                },
                {
                    "Name": "Others",
                    "OrderNo": 6
                }
            ]
        },
        {
            "Name": "Personal",
            "OrderNo": 2,
            "SubCategory": [
                {
                    "Name": "Personal",
                    "OrderNo": 1
                }
            ]
        },
        {
            "Name": "Transaction",
            "OrderNo": 3,
            "SubCategory": [
                {
                    "Name": "Transaction",
                    "OrderNo": 1
                }
            ]
        },
        {
            "Name": "Legal",
            "OrderNo": 4,
            "SubCategory": [
                {
                    "Name": "Legal",
                    "OrderNo": 1
                }
            ]
        },
        {
            "Name": "Property",
            "OrderNo": 5,
            "SubCategory": [
                {
                    "Name": "Property",
                    "OrderNo": 1
                }
            ]
        }
    ],
    "RequestTemplates": {
        "application": [
            {
                "name": "Conventional",
                "description": "Conventional - rate and term or cash out"
            },
            {
                "name": "FHA",
                "description": "FHA"
            },
            {
                "name": "FHA-S",
                "description": "FHA Streamline"
            },
            {
                "name": "VA",
                "description": "VA"
            },
            {
                "name": "VA-S",
                "description": "VA Streamline"
            }
        ]
    },
    "RequestStatuses": {
        "application": [
            {
                "Name": "Requested",
                "Label": "Requested"
            },
            {
                "Name": "Pending",
                "Label": "Pending"
            },
            {
                "Name": "Completed",
                "Label": "Accepted"
            },
            {
                "Name": "Rejected",
                "Label": "Not Accepted"
            }
        ]
    },
    "requestTypes": {
        "application": [
            {
                "groupName": "",
                "requestId": "agreenment-purchase-sale",
                "eventName": "DocumentAdded",
                "conditionCategory": "Transaction",
                "conditionSubCategory": "Transaction",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Purchase and Sale Agreement",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-APS",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Transaction"
                        ]
                    }
                },
                "isdefault": true,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "verification-mortgage",
                "eventName": "DocumentAdded",
                "conditionCategory": "Transaction",
                "conditionSubCategory": "Transaction",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Verification of Mortgage",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-VM",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Transaction"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "hazard-insurance",
                "eventName": "DocumentAdded",
                "conditionCategory": "Property",
                "conditionSubCategory": "Property",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Copy of Hazard Insurance",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-HI",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Property"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "verification-rent",
                "eventName": "DocumentAdded",
                "conditionCategory": "Transaction",
                "conditionSubCategory": "Transaction",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Verification of Rent (VOR)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-VOR",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Transaction"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "letter-giftor-gifted",
                "eventName": "DocumentAdded",
                "conditionCategory": "Personal",
                "conditionSubCategory": "Personal",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Gift Letter signed by Giftor and Gifted",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-VOR",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Personal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "verification-emd",
                "eventName": "DocumentAdded",
                "conditionCategory": "Transaction",
                "conditionSubCategory": "Transaction",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Verification of earnest money deposit (EMD)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-EMD",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Transaction"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "property-h06",
                "eventName": "DocumentAdded",
                "conditionCategory": "Property",
                "conditionSubCategory": "Property",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "H06 property insurance",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-H06",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Property"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "property-misc-dt",
                "eventName": "DocumentAdded",
                "conditionCategory": "Property",
                "conditionSubCategory": "Property",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Miscellaneous documentation as required by property type",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-MISCDT",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Property"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "property-misc-dp",
                "eventName": "DocumentAdded",
                "conditionCategory": "Property",
                "conditionSubCategory": "Property",
                "templateNames": [
                    "FHA-S",
                    "FHA",
                    "VA",
                    "Conventional",
                    "VA-S"
                ],
                "title": "Miscellaneous documentation as required by program requirements, e.g. co-op or condo",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-MISCDP",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Property"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-paystubs",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "W-2 & Pay Stubs",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional"
                ],
                "title": "Most recent 60 days of PayStubs",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-PayStubs",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Employment"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": true,
                "isExplanation": false,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-w2",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "W-2 & Pay Stubs",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional"
                ],
                "title": "Most recent 2 years W-2's",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-W2",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Employment"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": true,
                "isExplanation": false,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-ptr",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Tax Return",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional"
                ],
                "title": "Most recent two years personal tax returns (1040’s)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-PTR",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Tax Return"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": true,
                "isExplanation": false,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-pbftr",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Tax Return",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional"
                ],
                "title": "Most recent two years business federal tax return",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-PBFTR",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Tax Return"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": true,
                "isExplanation": false,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-k1",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Tax Return",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional"
                ],
                "title": "Most recent two years K1’s",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-K1",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Tax Return"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-pl-bl",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Tax Return",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional"
                ],
                "title": "YTD P&L and Balance Sheet (Un-audited)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-PLBL",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Tax Return"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-aoi",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Tax Return",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Articles of Incorporation to verify percentage of ownership of company",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-AOI",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Tax Return"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-por-ac",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Assets",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Proof of receipt of Alimony and Child support for the most recent 6-months. (if applicable)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-PORAC",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Assets"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": true,
                "isExplanation": false,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "legal-divorcedecree",
                "eventName": "DocumentAdded",
                "conditionCategory": "Legal",
                "conditionSubCategory": "Legal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Divorce decree or court order stipulation (all pages) (if applicable)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-DIVORCEDECREE",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Legal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "legal-coe",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Legal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Certificate of Eligibility (COE form 26-1880)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-COE",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Legal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "legal-dd214",
                "eventName": "DocumentAdded",
                "conditionCategory": "Legal",
                "conditionSubCategory": "Legal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Certificate of Release or Discharge from Active Duty (DD214 Form)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-DD214",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Legal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "legal-statement-service",
                "eventName": "DocumentAdded",
                "conditionCategory": "Legal",
                "conditionSubCategory": "Legal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Supply a statement of service signed by the adjutant, personal office, or commander of the unit.",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-LEGALSS",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Legal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "legal-social-security-letter",
                "eventName": "DocumentAdded",
                "conditionCategory": "Legal",
                "conditionSubCategory": "Legal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "A copy of Social Security award letter (if applicable)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-LEGALSS",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Legal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "personal-other-income",
                "eventName": "DocumentAdded",
                "conditionCategory": "Personal",
                "conditionSubCategory": "Personal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Provide documentation to source the other income 2 ",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-OTHERINCOME",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Personal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "personal-rac-gc",
                "eventName": "DocumentAdded",
                "conditionCategory": "Personal",
                "conditionSubCategory": "Personal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Resident Alien Card / Green Card",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-RACGC",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Personal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "legal-bankruptcy",
                "eventName": "DocumentAdded",
                "conditionCategory": "Legal",
                "conditionSubCategory": "Legal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Bankruptcy documents (if applicable)",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-BANKRUPTCY",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Legal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "transaction-closing-statement",
                "eventName": "DocumentAdded",
                "conditionCategory": "Transaction",
                "conditionSubCategory": "Transaction",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "A copy of final closing statement from sale",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-TCS",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Legal"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "personal-dl-pp",
                "eventName": "DocumentAdded",
                "conditionCategory": "Personal",
                "conditionSubCategory": "Personal",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Driver's License or Passport",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-TCS",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Personal"
                        ]
                    }
                },
                "isdefault": true,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-Adverse-credit-letter",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Income",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Letter of Explanation for Adverse credit",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-ACL",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Income"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-past-addresses-letter",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Others",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Letter of Explanation for past addresses within last 24months",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-PAL",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Others"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-credit-reports-letter",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Others",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Letter of Explanation for all AKA (other names) on Credit Reports",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-CRL",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Others"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-credit-enquiries-letter",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Others",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Letter of Explanation for all credit enquiries in last 6 months",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-CEL",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Others"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-mortgage-liabilities",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Others",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Mortgage Liabilities - Required most recent mortgage statement and current insurance declaration",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-ML",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Others"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "qualifying-transcripts-diploma",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Others",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Transcripts or Diploma",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-TOD",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Others"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "property-mi-cert",
                "eventName": "DocumentAdded",
                "conditionCategory": "Property",
                "conditionSubCategory": "Property",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Mortgage Insurance (MI) Cert",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-TOD",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Property"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "asset-bank-statements",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "Assets",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Two most recent bank statements.",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-ABS",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Assets"
                        ]
                    }
                },
                "isdefault": false,
                "isSyncRequired": true,
                "isExplanation": false,
                "isUpload": true
            },
            {
                "groupName": "",
                "requestId": "asset-bank-largedeposits",
                "eventName": "DocumentAdded",
                "conditionCategory": "Qualifying",
                "conditionSubCategory": "AssetsLargeDeposits",
                "templateNames": [
                    "FHA",
                    "VA",
                    "Conventional",
                    "FHA-S",
                    "VA-S"
                ],
                "title": "Letter of Explanation regarding bank transactions.",
                "description": "",
                "activity": {
                    "url": "activities/add-document.html",
                    "parameters": {
                        "documentName": "DOCITT-{entityId}-ABTLD",
                        "metadata": {
                            "requestId": "{id}"
                        },
                        "tags": [
                            "Assets",
                            "Large Deposits"
                        ]
                    }
                },
                "isDefault": false,
                "isSyncRequired": false,
                "isExplanation": true,
                "isUpload": true
            }
        ]
    },
    "database": "<databasename>"
}
	```


## Service Rules

NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by email service

- Database : MongoDB
- Collections : assignment

## External Services called

- Sendgrid : Version= [9.9.0] 
	* This library is used for send emails functionality.	
- MimeKit : Version= [2.0.2] 
	* MimeKit is a C# library which may be used for the creation and parsing of messages using the Multipurpose Internet Mail Extension (MIME).
- MailKit : Version= [2.0.2] 
	* MailKit is a cross-platform mail client library built on top of MimeKit.

## Environment Variables used by email service

- Envirounment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different environment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting envirounment variable in launchSettings.json.
- Below are the environment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\LendFoundry.Email.Api\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": "required-condition",
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://10.100.0.12:5008",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

## Changelog