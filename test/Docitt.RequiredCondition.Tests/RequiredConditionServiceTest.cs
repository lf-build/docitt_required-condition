﻿using Docitt.AssignmentEngine;
using Docitt.ReminderService.Services;
using Docitt.RequiredCondition.Tests.Fakes;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Docitt.RequiredCondition.Tests
{
    public class RequiredConditionServiceTest
    {
        [Fact]
        public void InitServiceWhenOperationOk()
        {
            var service = new RequiredConditionService
            (
                Mock.Of<IRequiredConditionRepository>(),
                Mock.Of<ILogger>(),
                Mock.Of<ITenantTime>(),
                Mock.Of<IEventHubClient>(),
                  Mock.Of<IEventHubClientFactory>(),
                Mock.Of<Configuration>(),
                Mock.Of<ILookupService>(),
                Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                  Mock.Of<IHttpContextAccessor>(),
                  Mock.Of<IReminderService>(),
                  Mock.Of<ICustomConditionRepository>(),
                   Mock.Of<IDecisionEngineService>(),                  
                      Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
            );

            Assert.IsType<RequiredConditionService>(service);
        }

        [Fact]
        public void InitServiceWithoutRepository()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new RequiredConditionService
                (
                    null,
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<IEventHubClient>(),
                       Mock.Of<IEventHubClientFactory>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ILookupService>(),
                        Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                    Mock.Of<IHttpContextAccessor>(),
                    Mock.Of<IReminderService>(),
                     Mock.Of<ICustomConditionRepository>(),
                        Mock.Of<IDecisionEngineService>(),
                           Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
                );
            });
        }

        [Fact]
        public void InitServiceWithoutLogger()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new RequiredConditionService
                (
                    Mock.Of<IRequiredConditionRepository>(),
                    null,
                    Mock.Of<ITenantTime>(),
                    Mock.Of<IEventHubClient>(),
                       Mock.Of<IEventHubClientFactory>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ILookupService>(),
                        Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                    Mock.Of<IHttpContextAccessor>(),
                     Mock.Of<IReminderService>(),
                      Mock.Of<ICustomConditionRepository>(),
                         Mock.Of<IDecisionEngineService>(),
                           Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
                );
            });
        }

        [Fact]
        public void InitServiceWithoutTenant()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new RequiredConditionService
                (
                    Mock.Of<IRequiredConditionRepository>(),
                    Mock.Of<ILogger>(),
                    null,
                    Mock.Of<IEventHubClient>(),
                      Mock.Of<IEventHubClientFactory>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ILookupService>(),
                        Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                    Mock.Of<IHttpContextAccessor>(),
                     Mock.Of<IReminderService>(),
                      Mock.Of<ICustomConditionRepository>(),
                       Mock.Of<IDecisionEngineService>(),
                         Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
                );
            });
        }

        [Fact]
        public void InitServiceWithoutEventhub()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new RequiredConditionService
                (
                    Mock.Of<IRequiredConditionRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    null,
                      Mock.Of<IEventHubClientFactory>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ILookupService>(),
                        Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                    Mock.Of<IHttpContextAccessor>(),
                     Mock.Of<IReminderService>(),
                      Mock.Of<ICustomConditionRepository>(),
                       Mock.Of<IDecisionEngineService>(),
                         Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
                );
            });
        }

        [Fact]
        public void InitServiceWithoutEventhubFactory()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new RequiredConditionService
                (
                    Mock.Of<IRequiredConditionRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                     Mock.Of<IEventHubClient>(),
                     null,
                    Mock.Of<Configuration>(),
                    Mock.Of<ILookupService>(),
                        Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                    Mock.Of<IHttpContextAccessor>(),
                     Mock.Of<IReminderService>(),
                      Mock.Of<ICustomConditionRepository>(),
                       Mock.Of<IDecisionEngineService>(),
                         Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
                );
            });
        }

        [Fact]
        public void InitServiceWithoutConfiguration()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new RequiredConditionService
                (
                    Mock.Of<IRequiredConditionRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<IEventHubClient>(),
                       Mock.Of<IEventHubClientFactory>(),
                    null,
                    Mock.Of<ILookupService>(),
                        Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                    Mock.Of<IHttpContextAccessor>(),
                     Mock.Of<IReminderService>(),
                      Mock.Of<ICustomConditionRepository>(),
                       Mock.Of<IDecisionEngineService>(),
                         Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
                );
            });
        }

        [Fact]
        public void InitServiceWithoutLookupEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new RequiredConditionService
                (
                    Mock.Of<IRequiredConditionRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<IEventHubClient>(),
                       Mock.Of<IEventHubClientFactory>(),
                    Mock.Of<Configuration>(),
                    null,
                        Mock.Of<IDocumentGeneratorService>(),
                 Mock.Of<IDocumentManagerService>(),
                    Mock.Of<IHttpContextAccessor>(),
                     Mock.Of<IReminderService>(),
                      Mock.Of<ICustomConditionRepository>(),
                       Mock.Of<IDecisionEngineService>(),
                            Mock.Of<ITokenReader>(),
                        Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>()
                );
            });
        }

        [Fact]
        public void AddWhenInvalidEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Add("", "", "document", Mock.Of<RequestPayload>());
            });
        }

        [Fact]
        public void AddWhenInvalidEntityId()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Add("merchant", "", "document", Mock.Of<RequestPayload>());
            });
        }

        [Fact]
        public void AddWhenInvalidActionType()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Add("merchant", "123", "", Mock.Of<RequestPayload>());
            });
        }

        [Fact]
        public void AddWhenInvalidPayload()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Add("merchant", "123", "document", null);
            });
        }

        [Fact]
        public void AddWhenNoConfiguration()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var config = new Configuration();
                var service = GetService(null, config);
                service.Add("merchant", "123", "document", Mock.Of<RequestPayload>());
            });
        }

        [Fact]
        public void AddWhenConfigurationDontHaveRequestTypeKey()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService(null);
                service.Add("loan", "123", "document", Mock.Of<RequestPayload>());
            });
        }

        [Fact]
        public void AddWhenExecutionOk()
        {
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High
            };
            service.Add("merchant", "123", "document", payload);

            var data = service.GetBy("merchant");
            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.Equal("123", data.First().EntityId);
        }

        [Fact]
        public void GetByEntityAndActionWhenInvalidEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("", "document");
            });
        }

        [Fact]
        public void GetByEntityAndActionWhenInvalidActionType()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("merchant", "");
            });
        }

        [Fact]
        public void GetEntityAndActionWhenItHasData()
        {
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High
            };
            service.Add("merchant", "123", "document", payload);

            var data = service.GetBy("merchant", "document");
            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.Equal("123", data.First().EntityId);
        }

        [Fact]
        public void GetEntityAndActionWhenItHasNoData()
        {
            var service = GetService();
            var data = service.GetBy("merchant", "document");
            Assert.NotNull(data);
            Assert.Empty(data);
        }

        [Fact]
        public void GetByEntityWhenInvalidEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("");
            });
        }

        [Fact]
        public void GetByEntityWhenItHasData()
        {
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High
            };
            service.Add("merchant", "123", "document", payload);

            var data = service.GetBy("merchant");
            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.Equal("123", data.First().EntityId);
        }

        [Fact]
        public void GetByEntityWhenItHasNoData()
        {
            var service = GetService();
            var data = service.GetBy("merchant");
            Assert.NotNull(data);
            Assert.Empty(data);
        }

        [Fact]
        public void GetSummaryByEntityAndActionWhenInvalidEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetSummary("", "document");
            });
        }

        [Fact]
        public void GetSummaryByEntityAndActionWhenInvalidAction()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetSummary("merchant", "");
            });
        }

        [Fact]
        public void GetSummaryByEntityAndActionWhenItHasData()
        {
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High
            };
            service.Add("merchant", "123", "document", payload);
            service.Add("merchant", "123", "document", payload);
            service.Add("merchant", "123", "document", payload);

            var data = service.GetSummary("merchant", "document");
            Assert.NotNull(data);
            Assert.Equal(3, data.Requested);
        }

        [Fact]
        public void GetSummaryByEntityAndActionWhenItHasNoData()
        {
            var service = GetService();
            var data = service.GetSummary("merchant", "document");
            Assert.Equal(0, data.Cancelled);
            Assert.Equal(0, data.Completed);
            Assert.Equal(0, data.Requested);
        }

        [Fact]
        public void GetSummaryByEntityWhenInvalidEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetSummary("");
            });
        }

        [Fact]
        public void GetSummaryByEntityWhenItHasData()
        {
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High
            };
            service.Add("merchant", "123", "document", payload);
            service.Add("merchant", "123", "document", payload);
            service.Add("merchant", "123", "document", payload);
            service.Add("merchant", "123", "document", payload);
            service.Add("merchant", "123", "document", payload);

            var data = service.GetSummary("merchant");
            Assert.NotNull(data);
            Assert.Equal(5, data.Requested);
        }

        [Fact]
        public void GetSummaryByEntityWhenItHasNoData()
        {
            var service = GetService();
            var data = service.GetSummary("merchant");
            Assert.Equal(0, data.Requested);
            Assert.Equal(0, data.Completed);
            Assert.Equal(0, data.Cancelled);
        }

        [Fact]
        public void GetGroupSummaryWhenInvalidEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetGroupSummary("", new[] { "123" });
            });
        }

        [Fact]
        public void GetGroupSummaryWhenInvalidEntityId()
        {
            var service = GetService();
            var data = service.GetGroupSummary("merchant", null);
            Assert.NotNull(data);
            Assert.Equal(0, data.First().Total);
            Assert.Equal("groupDocument", data.First().Name);
        }

        [Fact]
        public void GetGroupSummaryWhenItHasData()
        {
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High
            };
            service.Add("merchant", "123", "document", payload);
            service.Add("merchant", "123", "document", payload);

            var data = service.GetGroupSummary("merchant", new[] { "123" });
            Assert.NotNull(data);
            Assert.Equal("groupDocument", data.First().Name);
            Assert.Equal(2, data.First().Total);
        }

        [Fact]
        public void GetGroupSummaryWhenItHasNoData()
        {
            var service = GetService();
            var data = service.GetGroupSummary("merchant", new[] { "123" });
            Assert.NotNull(data);
            Assert.Equal(0, data.First().Total);
        }

        [Fact]
        public void GetGroupSummaryWhenMultipleIds()
        {
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High
            };

            service.Add("merchant", "111", "document", payload);

            service.Add("merchant", "222", "document", payload);

            service.Add("merchant", "333", "document", payload);

            var data = service.GetGroupSummary("merchant", new[] { "111", "333" });
            Assert.NotNull(data);
            Assert.Equal("groupDocument", data.First().Name);
            Assert.Equal(2, data.First().Total);
        }

        [Fact]
        public void DeleteWhenInvalidEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Delete("", "document", "111");
            });
        }

        [Fact]
        public void DeleteWhenInvalidAction()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Delete("", "document", "111");
            });
        }

        [Fact]
        public void DeleteWhenInvalidRequestId()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Delete("merchant", "document", "");
            });
        }

        [Fact]
        public void DeleteWhenExecutionOk()
        {
            // addd a new request
            var _entity = "merchant";
            var _requestId = "document";
            var _entityId = "111";
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High,
            };
            service.Add(_entity, _entityId, _requestId, payload);

            // search request in order to see if it was added properly
            var result1 = service.GetBy(_entity);
            Assert.NotNull(result1);
            Assert.NotEmpty(result1);
            Assert.Equal(1, result1.Count());
            Assert.Equal("111", result1.First().EntityId);

            // delete actual request
            service.Delete(_entity, _entityId, _requestId);

            // try get it back in order to make sure if it was deleted
            var result2 = service.GetBy(_entity);
            Assert.NotNull(result2);
            Assert.Empty(result2);
        }

        [Fact]
        public void DeleteWhenItemNotFound()
        {
            // addd a new request
            var _entity = "merchant";
            var _actionType = "document";
            var _entityId = "111";
            var service = GetService();
            var payload = new RequestPayload
            {
                Data = new { name = "maria" },
                DueDate = DateTime.Now,
                Priority = RequestPriority.High,
            };
            service.Add(_entity, _entityId, _actionType, payload);

            // search request in order to see if it was added properly
            var result1 = service.GetBy(_entity);
            Assert.NotNull(result1);
            Assert.NotEmpty(result1);
            Assert.Equal(1, result1.Count());
            Assert.Equal("111", result1.First().EntityId);

            // try to delete an non-exist object
            Assert.Throws<NotFoundException>(() =>
            {
                service.Delete(_entity, _actionType, "00000000001");
            });
        }

        [Fact]
        public void GetByEntityActionRequestIdWhenNoEntityInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("", "action", "123");
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy(string.Empty, "action", "123");
            });
        }

        [Fact]
        public void GetByEntityActionRequestIdWhenNoActionTypeInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("application", "", "123");
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("application", string.Empty, "123");
            });
        }

        [Fact]
        public void GetByEntityActionRequestIdWhenNoRequestIdInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("application", "action", "");
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("application", "action", string.Empty);
            });
        }

        [Fact]
        public void GetByEntityActionRequestIdWhenDataFound()
        {
            var _entityType = "application";
            var _actionType = "_actionType";
            var repo = new FakeRepository();
            repo.Add(new Request
            {
                EntityType = _entityType,
                RequestType = new RequestType
                {
                    RequestId = _actionType,
                    Activity = new Activity
                    {
                        Parameters = "{ 'name' : 'bill'}"
                    }
                }
            });

            var service = GetService(repo);
            var request = service.GetBy(_entityType).FirstOrDefault();

            Assert.NotNull(request);

            var result = service.GetBy(request.EntityType, request.RequestType.RequestId, request.Id);

            Assert.NotNull(result);
            Assert.Equal(_entityType, result.EntityType);
            Assert.Equal(_actionType, result.RequestType.RequestId);
            Assert.Equal(request.Id, result.Id);
        }

        [Fact]
        public void GetByExpressionWhenNoEntityType()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("", "actionType", "expBody", new[] { "expValues" });
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy(string.Empty, "actionType", "expBody", new[] { "expValues" });
            });
        }

        [Fact]
        public void GetByExpressionWhenNoExpressionBody()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("entityType", "actionType", "", new[] { "expValues" });
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("entityType", "actionType", string.Empty, new[] { "expValues" });
            });
        }

        [Fact]
        public void GetByExpressionWhenNoExpressionValues()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetBy("entityType", "actionType", "expBody", null);
            });
        }

        [Fact]
        public void GetByExpressionWhenDataFoundButNoMatchedList()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var _entityType = "application";
                var _actionType = "_actionType";
                var repo = new FakeRepository();
                repo.Add(new Request
                {
                    EntityType = _entityType,
                    RequestType = new RequestType
                    {
                        RequestId = _actionType,
                        Activity = new Activity
                        {
                            Parameters = "{ 'name' : 'bill'}"
                        }
                    },
                    Data = "{'name':'bill'}"
                });

                var service = GetService(repo);
                var result = service.GetBy(_entityType, _actionType, "name", new[] { "carla" });

                Assert.NotNull(result);
                Assert.Empty(result);
            });
        }

        [Fact]
        public void GetByExpressionWehnDataFoundAndMatches()
        {
            var _entityType = "application";
            var _actionType = "_actionType";
            var repo = new FakeRepository();
            repo.Add(new Request
            {
                EntityType = _entityType,
                RequestType = new RequestType
                {
                    RequestId = _actionType,
                    Activity = new Activity
                    {
                        Parameters = "{ 'name' : 'bill'}"
                    }
                },
                Data = "{'name':'bill'}"
            });

            var service = GetService(repo);
            var result = service.GetBy(_entityType, _actionType, "name", new[] { "bill" });

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetByStatusEntityActionAndStatusWhenNoEntityInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByStatus("", "action", Status.Pending.ToString());
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByStatus(string.Empty, "action", Status.Pending.ToString());
            });
        }

        [Fact]
        public void GetByStatusEntityActionAndStatusWhenNoActionInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByStatus("application", "", Status.Pending.ToString());
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByStatus("application", string.Empty, Status.Pending.ToString());
            });
        }

        [Fact]
        public void GetByEntityActionAndStatusWhenNoDataFound()
        {
            var repository = new FakeRepository();
            var service = GetService(repository);
            var data = service.GetByStatus("application", "action", Status.Pending.ToString());
            Assert.Empty(data);
        }

        [Fact]
        public void GetByEntityTypeEntityIdAndStatusWhenDataFound()
        {
            var repository = new FakeRepository();
            var requests = new[] {
                new Request {
                    EntityType = "application",
                    EntityId = "action",
                    RequestType = new RequestType {
                        RequestId = "action",
                        Activity = new Activity {
                            Parameters = null
                        }
                    },
                    Status =Status.Pending.ToString(),
                }
            };

            requests.ToList().ForEach(x => repository.Add(x));
            var service = GetService(repository);
            var data = service.GetByStatus("application", "action", Status.Pending.ToString());
            Assert.Equal(1, data.Count());
            Assert.True(data.ToList().TrueForAll(x => x.Status == Status.Pending.ToString()));
        }

        [Fact]
        public void GetByEntityAndStatusWhenNoEntityInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByStatus("", Status.Pending.ToString());
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByStatus(string.Empty, Status.Pending.ToString());
            });
        }

        [Fact]
        public void GetByEntityAndStatusWhenNoDataFound()
        {
            var repository = new FakeRepository();

            var service = GetService(repository);
            var data = service.GetByStatus("application", Status.Pending.ToString());

            Assert.Empty(data);
        }

        [Fact]
        public void GetByEntityAndStatusWhenDataFound()
        {
            var repository = new FakeRepository();
            var requests = new[] {
                new Request {
                    EntityType = "merchant",
                    RequestType = new RequestType {
                        RequestId = "action",
                        Activity = new Activity {
                            Parameters = null
                        }
                    },
                    Status = Status.Pending.ToString(),
                }
            };
            requests.ToList().ForEach(x => repository.Add(x));
            var service = GetService(repository);
            var data = service.GetByStatus("merchant", Status.Pending.ToString());

            Assert.NotEmpty(data);
            Assert.Equal(1, data.Count());
            Assert.True(data.ToList().TrueForAll(x => x.Status == Status.Pending.ToString()));
        }

        private IRequiredConditionService GetService(IRequiredConditionRepository repository = null, Configuration configuration = null, ILookupService lookupEntity = null)
        {
            IRequiredConditionRepository inTimeRepository = repository;
            if (inTimeRepository == null)
                inTimeRepository = new FakeRepository();

            Configuration inTimeConfiguration = configuration;
            if (inTimeConfiguration == null)
                inTimeConfiguration = new Configuration
                {
                    RequestTypes = new Dictionary<string, IEnumerable<RequestType>>
                    {
                        {
                            "merchant",  new[]
                            {
                                new RequestType() {
                                    GroupName = "groupDocument",
                                    RequestId = "document",
                                    Activity = new Activity {
                                        Parameters = new { key = 1 }
                                    }
                                }
                            }
                        }
                    },
                    RequestStatuses = new Dictionary<string, IEnumerable<RequestStatus>>
                    {
                        {
                            "merchant",  new[]
                            {
                                new RequestStatus() {
                                   Label = "Pending",
                                   Name = "Pending"
                                },
                                 new RequestStatus() {
                                   Label = "Requested",
                                   Name = "Requested"
                                },
                                  new RequestStatus() {
                                   Label = "Accepted",
                                   Name = "Completed"
                                },
                                  new RequestStatus() {
                                   Label = "Rejected",
                                   Name = "Rejected"
                                },
                            }
                        }
                    }
                };

            ILookupService inTimeLookupEntity = lookupEntity;
            if (inTimeLookupEntity == null)
            {
                var mockLookup = new Mock<ILookupService>();
                mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new Dictionary<string, string>
                {
                    { "loan","loan" },
                    { "merchant","merchant" },
                    { "application","application" },
                    { "borrower","borrower" },
                });
                inTimeLookupEntity = mockLookup.Object;
            }

            return new RequiredConditionService(inTimeRepository, Mock.Of<ILogger>(), Mock.Of<ITenantTime>(), Mock.Of<IEventHubClient>(), Mock.Of<IEventHubClientFactory>(), inTimeConfiguration, inTimeLookupEntity, Mock.Of<IDocumentGeneratorService>(), Mock.Of<IDocumentManagerService>(), Mock.Of<IHttpContextAccessor>(), Mock.Of<IReminderService>(), Mock.Of<ICustomConditionRepository>() ,Mock.Of<IDecisionEngineService>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>(),
                         Mock.Of<IAssignmentService>());
        }

        [Fact]
        public void GetRequestTypesThrowsArgumentException()
        {
            var config = new Configuration
            {
                RequestTypes = new Dictionary<string, IEnumerable<RequestType>>
                {
                    {
                        "merchant", new[]
                        {
                            new RequestType()
                            {
                                GroupName = "groupDocument",
                                RequestId = "document",
                                Activity = new Activity
                                {
                                    Parameters = new {key = 1}
                                }
                            }
                        }
                    }
                }
            };
            var service = GetService(null, config);
            Assert.Throws<ArgumentException>(() =>
            {
                service.GetRequestTypes(null);
            });
        }

        [Fact]
        public void GetRequestTypesThrowsNotFoundException()
        {
            var config = new Configuration
            {
                RequestTypes = new Dictionary<string, IEnumerable<RequestType>>
                {
                    {
                        "merchant", new[]
                        {
                            new RequestType()
                            {
                                GroupName = "groupDocument",
                                RequestId = "document",
                                Activity = new Activity
                                {
                                    Parameters = new {key = 1}
                                }
                            }
                        }
                    }
                }
            };
            var service = GetService(null, config);
            Assert.Throws<NotFoundException>(() =>
            {
                service.GetRequestTypes("application");
            });
        }

        [Fact]
        public void GetRequestTypesReturnsTypesWhenFound()
        {
            var config = new Configuration
            {
                RequestTypes = new Dictionary<string, IEnumerable<RequestType>>
                {
                    {
                        "merchant", new[]
                        {
                            new RequestType()
                            {
                                GroupName = "groupDocument",
                                RequestId = "document",
                                Activity = new Activity
                                {
                                    Parameters = new {key = 1}
                                }
                            },
                            new RequestType()
                            {
                                GroupName = "groupDocument1",
                                RequestId = "document2",
                                Activity = new Activity
                                {
                                    Parameters = new {key = 2}
                                }
                            }
                        }
                    }
                }
            };
            var service = GetService(null, config);
            var requestTypes = service.GetRequestTypes("merchant").Result;
            Assert.Equal(2, requestTypes.Count());
        }
    }
}