﻿using LendFoundry.EventHub.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition.Tests.Fakes
{
    public class FakeEventHub : IEventHubClient
    {
        public EventInfo InMemoryData { get; set; }

        public void On(string eventName, Action<EventInfo> handler)
        {
            handler.Invoke(InMemoryData);
        }

        public Task<bool> Publish<T>(T @event)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Publish<T>(string eventName, T @event)
        {
            throw new NotImplementedException();
        }

        public void PublishBatchWithInterval<T>(List<T> events, int interval = 100)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
        }

        public void StartAsync()
        {
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public void UnSubscribeAllEvents()
        {
            throw new NotImplementedException();
        }
    }
}