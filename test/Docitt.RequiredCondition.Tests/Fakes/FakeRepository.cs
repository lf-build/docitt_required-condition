﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition.Tests.Fakes
{
    public class FakeRepository : IRequiredConditionRepository
    {
        public List<IRequest> InMemoryData { get; set; } = new List<IRequest>();

        public void Add(IRequest item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            InMemoryData.Add(item);
        }

        public Task<IEnumerable<IRequest>> All(Expression<Func<IRequest, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return InMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public int Count(Expression<Func<IRequest, bool>> query)
        {
            return InMemoryData
                         .AsQueryable()
                         .Count(query);
        }

        public Task<IRequest> Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IRequest> GetManyBy(string eventName)
        {
            return InMemoryData
                 .AsQueryable()
                 .Where(r => r.RequestType.EventName == eventName);
        }

        public IEnumerable<IRequest> GetManyByStatus(string entityType, string status)
        {
            return InMemoryData
                    .AsQueryable()
                    .Where(r => r.EntityType == entityType && r.Status == status);
        }

        public IEnumerable<IRequest> GetManyBy(string entityType, string requestId)
        {
            return InMemoryData
                    .AsQueryable()
                    .Where(r => r.EntityType == entityType && r.RequestType.RequestId == requestId);
        }

        public IEnumerable<IRequest> GetManyByStatus(string entityType, string entityId, string status)
        {
            return InMemoryData
                 .AsQueryable()
                 .Where(r => r.EntityType == entityType && r.EntityId == entityId && r.Status == status);
        }

        public IEnumerable<IRequest> GetManyByEntityType(string entityType)
        {
            return InMemoryData
                  .AsQueryable()
                  .Where(r => r.EntityType == entityType);
        }

        public IRequest GetById(string entityType, string entityId, string requestId)
        {
            return InMemoryData
                 .AsQueryable()
                 .FirstOrDefault(r => r.EntityType == entityType && r.EntityId == entityId && r.RequestType.RequestId == requestId);
        }

        public IRequest GetSubmittedById(string entityType, string entityId, string requestId)
        {
            return InMemoryData
                 .AsQueryable()
                 .FirstOrDefault(r => r.EntityType == entityType && r.EntityId == entityId && r.RequestType.RequestId == requestId && r.Status == Status.Pending.ToString());
        }

        public void Remove(IRequest item)
        {
            InMemoryData = InMemoryData.Where(x => x.Id != item.Id).ToList();
        }

        public void Update(IRequest item)
        {
            var index = InMemoryData.IndexOf(item);
            InMemoryData[index] = item;
        }

        public Task ChangeStatus(string requestId, IRequest request)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IRequest> GetManyBy(string entityType, Status status)
        {
            return InMemoryData
                 .AsQueryable()
                 .Where(r => r.EntityType == entityType && r.Status == status.ToString());
        }

        public Task MarkAsSkipped(string requestId, bool skipped)
        {
            throw new NotImplementedException();
        }
    }
}