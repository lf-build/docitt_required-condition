﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using System;
namespace Docitt.RequiredCondition.Client
{
    public static class RequiredConditionClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddRequiredConditionService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IRequiredConditionClientFactory>(p => new RequiredConditionClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IRequiredConditionClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddRequiredConditionService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IRequiredConditionClientFactory>(p => new RequiredConditionClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IRequiredConditionClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddRequiredConditionService(this IServiceCollection services)
        {
            services.AddTransient<IRequiredConditionClientFactory>(p => new RequiredConditionClientFactory(p));
            services.AddTransient(p => p.GetService<IRequiredConditionClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

      
    }
}