﻿using LendFoundry.Security.Tokens;
namespace Docitt.RequiredCondition.Client
{
    public interface IRequiredConditionClientFactory
    {
        IRequiredConditionService Create(ITokenReader reader);
    }
}