﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Client;
using RestSharp;
using RestSharp.Extensions;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition.Client
{
    public class RequiredConditionServiceClient : IRequiredConditionService
    {
        public RequiredConditionServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public void Add(string entityType, string entityId, string actionType, IRequestPayload request,
            bool isDefaultCondition, List<string> sendToList = null)
        {
            Client.PostAsync($"/{entityType}/{entityId}/{actionType}/", request, true);
        }

        public void Delete(string entityType, string entityId, string requestId)
        {
            Client.DeleteAsync($"/{entityType}/{entityId}/{requestId}/");
        }

        public IEnumerable<IRequest> GetBy(string entityType)
        {
            return Client.GetAsync<List<Request>>($"/{entityType}/").Result;
        }

        public IEnumerable<IRequest> GetBy(string entityType, string requestId)
        {
            return Client.GetAsync<List<Request>>($"/{entityType}/{requestId}/").Result;
        }

        public IRequest GetBy(string entityType, string requestId, string id)
        {
            return Client.GetAsync<Request>($"/{entityType}/{requestId}/{id}/").Result;
        }

        public IRequestSummary GetSummary(string entityType, string requestId)
        {
            return Client.GetAsync<RequestSummary>($"/{entityType}/{requestId}/count").Result;
        }

        public IRequestSummary GetSummary(string entityType)
        {
            return Client.GetAsync<RequestSummary>($"/{entityType}/count").Result;
        }

        public IEnumerable<IRequest> GetBy(string entityType, string requestId, string expressionBody,
            IEnumerable<string> expressionValues)
        {
            var request = new RestRequest("/{entityType}/{requestId}/filter/{expressionBody}/{*expressionValues}",
                Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("requestId", requestId);
            request.AddUrlSegment("expressionBody", expressionBody);
            var strBuilder = new StringBuilder();
            foreach (var item in expressionValues) strBuilder.Append($"{item}/");
            request.AddUrlSegment("expressionValues", strBuilder.ToString());
            return Client.Execute<IEnumerable<Request>>(request);
        }

        public IEnumerable<IGroupSummary> GetGroupSummary(string entityType, IEnumerable<string> entityIds)
        {
            var request = new RestRequest("/{entityType}/groups/{*entityIds}/", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var strBuilder = new StringBuilder();
            foreach (var item in entityIds) strBuilder.Append($"{item}/");
            request.AddUrlSegment("entityIds", strBuilder.ToString());
            return Client.Execute<IEnumerable<GroupSummary>>(request);
        }

        public async Task<IEnumerable<IRequestType>> GetRequestTypes(string entityType)
        {
            return await Client.GetAsync<List<RequestType>>($"/{entityType}/request-types");
        }

        public IEnumerable<IRequest> GetByStatus(string entityType, string entityId, string status)
        {
            return Client.GetAsync<List<Request>>($"/{entityType}/{entityId}/status/{status}").Result;
        }

        public async Task<List<string>> GetCategories(string entityType)
        {
            return await Client.GetAsync<List<string>>($"/{entityType}/categories");
        }

        public IEnumerable<IRequest> GetByStatus(string entityType, string status)
        {
            return Client.GetAsync<List<Request>>($"/{entityType}/status/{status}").Result;
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByEntity(string entityType, string entityId)
        {
            return await Client.GetAsync<List<Request>>($"/{entityType}/{entityId}");
        }

        public async Task SubmitDocumentWithExplanation(string entityType, string entityId, string requestId,
            IList<DocumentDetails> files, object metadata, string explanation)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{requestId}/submit", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("requestId", requestId);
            if (metadata != null)
                request.AddParameter("metadata", metadata);
            request.AddParameter("explanation", explanation);

            foreach (var file in files)
            {
                request.AddFile("files", file.Content.ReadAsBytes(), file.FileName);
            }

            await Client.ExecuteAsync(request);
        }

        public async Task<IEnumerable<IDocument>> GetDocumentList(string entityType, string entityId, string requestId)
        {
            return await Client.GetAsync<List<Document>>($"/{entityType}/{entityId}/{requestId}/documents");
        }

        public async Task AddDefault(string entityType, string entityId, IRequestPayload payload)
        {
            await Client.PutAsync($"/{entityType}/{entityId}/", payload, true);
        }

        public async Task Accept(string entityType, string entityId, IAcceptRequest acceptRequest)
        {
            await Client.PutAsync($"/{entityType}/{entityId}/accept", acceptRequest, true);
        }

        public async Task Reject(string entityType, string entityId, IRejectRequest rejectRequest)
        {
            await Client.PutAsync($"/{entityType}/{entityId}/reject", rejectRequest, true);
        }

        public async Task<IEnumerable<IRequestTemplate>> GetRequestTemplates(string entityType)
        {
            return await Client.GetAsync<List<RequestTemplate>>($"/{entityType}/request-templates");
        }

        public async Task<ICustomCondition> AddCustom(string entityType, string entityId, ICustomRequestPayload payload)
        {
            return await Client.PostAsync<ICustomRequestPayload, CustomCondition>(
                $"/{entityType}/{entityId}/custom-condition", payload, true);
        }

        public async Task DeleteCustom(string entityType, string entityId, string customRequestId)
        {
            var requestId = customRequestId;
            await Client.DeleteAsync($"/{entityType}/{entityId}/{requestId}/custom");
        }

        public async Task MarkAsSkipped(string entityType, string entityId, string requestId)
        {
            await Client.PutAsync<dynamic>($"/{entityType}/{entityId}/{requestId}/mark-as-skipped", null, true);
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByUser(string entityType, string entityId)
        {
            return await Client.GetAsync<List<Request>>($"/{entityType}/{entityId}/mine");
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByUserByStatus(string entityType, string entityId,
            string status)
        {
            return await Client.GetAsync<List<Request>>($"/{entityType}/{entityId}/mine/status/{status}");
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByUserWithSpouse(string entityType, string entityId)
        {
            return await Client.GetAsync<List<Request>>($"/{entityType}/{entityId}/mine-with-spouse");
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByUserWithSpouseByStatus(string entityType, string entityId,
            string status)
        {
            return await Client.GetAsync<List<Request>>($"/{entityType}/{entityId}/mine-with-spouse/status/{status}");
        }

        public async Task UpdateInviteIdWithUserName(string entityType, string entityId, string inviteId)
        {
            await Client.PutAsync<dynamic>($"/{entityType}/{entityId}/update-recipients-from/{inviteId}/to-username", null, true);
        }
    }
}