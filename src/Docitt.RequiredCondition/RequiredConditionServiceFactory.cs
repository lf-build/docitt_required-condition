﻿using Docitt.AssignmentEngine;
using Docitt.ReminderService.Services;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace Docitt.RequiredCondition
{
    public class RequiredConditionServiceFactory : IRequiredConditionServiceFactory
    {
        public RequiredConditionServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IRequiredConditionService Create(StaticTokenReader reader, ILogger logger)
        {
            return new RequiredConditionService
            (
                repository: Provider.GetService<IRequiredConditionRepositoryFactory>().Create(reader),
                logger: logger,
                tenantTime: Provider.GetService<ITenantTime>(),
                eventhub: Provider.GetService<IEventHubClient>(),
                configuration: Provider.GetService<Configuration>(),
                lookup: Provider.GetService<ILookupService>(),
                documentGeneratorService: Provider.GetService<IDocumentGeneratorService>(),
                documentManagerService: Provider.GetService<IDocumentManagerService>(),
                httpAccessor: Provider.GetService<IHttpContextAccessor>(),
               reminderService: Provider.GetService<IReminderService>(),
               customConditionRepository: Provider.GetService<ICustomConditionRepositoryFactory>().Create(reader),
               decisionEngine: Provider.GetService<IDecisionEngineService>(),
               eventHubFactory: Provider.GetService<IEventHubClientFactory>(),
                tokenReader: Provider.GetService<ITokenReader>(),
                tokenParser: Provider.GetService<ITokenHandler>(),
                assignmentService: Provider.GetService<IAssignmentService>()

            );
        }
    }
}