﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Docitt.AssignmentEngine;
using Docitt.ReminderService.Services;
using Docitt.Questionnaire;

namespace Docitt.RequiredCondition
{
    public class RequiredConditionService : IRequiredConditionService
    {
        public RequiredConditionService
        (
            IRequiredConditionRepository repository,
            ILogger logger,
            ITenantTime tenantTime,
            IEventHubClient eventhub,
            IEventHubClientFactory eventHubFactory,
            Configuration configuration,
            ILookupService lookup,
            IDocumentGeneratorService documentGeneratorService,
            IDocumentManagerService documentManagerService,
            IHttpContextAccessor httpAccessor,
            IReminderService reminderService,
            ICustomConditionRepository customConditionRepository,
            IDecisionEngineService decisionEngine,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IAssignmentService assignmentService
        )
        {
            Repository = repository ?? throw new ArgumentException($"{nameof(repository)} is mandatory", nameof(repository));
            TenantTime = tenantTime ?? throw new ArgumentException($"{nameof(tenantTime)} is mandatory", nameof(tenantTime));
            EventHub = eventhub ?? throw new ArgumentException($"{nameof(eventhub)} is mandatory", nameof(eventhub));
            EventHubFactory = eventHubFactory ?? throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory", nameof(eventHubFactory));
            Configuration = configuration ?? throw new ArgumentException($"{nameof(configuration)} is mandatory", nameof(configuration));
            Lookup = lookup ?? throw new ArgumentException($"{nameof(lookup)} is mandatory", nameof(lookup));
            HttpAccessor = httpAccessor ?? throw new ArgumentException($"{nameof(httpAccessor)} is mandatory", nameof(httpAccessor));
            Logger = logger ?? throw new ArgumentException($"{nameof(logger)} is mandatory", nameof(logger));
            DocumentGenerator = documentGeneratorService ??
                                throw new ArgumentException($"{nameof(documentGeneratorService)} is mandatory", nameof(documentGeneratorService));
            DocumentManager = documentManagerService ??
                              throw new ArgumentException($"{nameof(documentManagerService)} is mandatory", nameof(documentManagerService));
            ReminderService = reminderService ?? throw new ArgumentException($"{nameof(reminderService)} is mandatory", nameof(reminderService));
            CustomConditionRepository = customConditionRepository ??
                                        throw new ArgumentException(
                                            $"{nameof(customConditionRepository)} is mandatory", nameof(customConditionRepository));
            DecisionEngine = decisionEngine ?? throw new ArgumentException($"{nameof(decisionEngine)} is mandatory");
            TokenReader = tokenReader ?? throw new ArgumentException($"{nameof(tokenReader)} is mandatory", nameof(tokenReader));
            TokenParser = tokenParser ?? throw new ArgumentException($"{nameof(tokenParser)} is mandatory", nameof(tokenParser));
            AssignmentService = assignmentService ??
                                throw new ArgumentException($"{nameof(assignmentService)} is mandatory", nameof(assignmentService));
        }


        private IDecisionEngineService DecisionEngine { get; }
        private ILogger Logger { get; }
        private ICustomConditionRepository CustomConditionRepository { get; }
        private IHttpContextAccessor HttpAccessor { get; }
        private IDocumentGeneratorService DocumentGenerator { get; }
        private IDocumentManagerService DocumentManager { get; }
        private ILookupService Lookup { get; }
        private Configuration Configuration { get; }
        private IEventHubClient EventHub { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantTime TenantTime { get; }
        private IRequiredConditionRepository Repository { get; }
        private IReminderService ReminderService { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }
        private IAssignmentService AssignmentService { get; }

        public void Add(string entityType, string entityId, string requestId, IRequestPayload payload,
            bool isDefaultCondition = false, List<string> sendToList = null)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            if (string.IsNullOrWhiteSpace(requestId))
                throw new ArgumentException($"{nameof(requestId)} is mandatory", nameof(requestId));

            if (Configuration.RequestTypes == null)
                throw new NotFoundException("Service seems to be not configured, please check");

            if (!Configuration.RequestTypes.ContainsKey(entityType))
                throw new NotFoundException($"No configuration found to {entityType}");

            EnsureDataIsValid(payload);
            entityType = EnsureEntityTypeIsValid(entityType);

            if (Configuration.RequestStatuses == null)
                throw new NotFoundException("statuses not found in service configuration, please check");

            var requestStatus = GetRequestStatus(entityType, Status.Requested.ToString());
            if (requestStatus == null)
                throw new NotFoundException(
                    $"{Status.Requested.ToString()} status not found in service configuration, please check");

            //if condition is default added (Post -application) that time need to add source by system 
            if (isDefaultCondition && Configuration.DefaultUserInformation != null)
            {
                payload.SourceBy = Configuration.DefaultUserInformation.UserName;
                payload.Role = Configuration.DefaultUserInformation.UserRole;
            }

            var metadata = new Request
            {
                RequestedDate = new TimeBucket(TenantTime.Now),
                DueDate = new TimeBucket(payload.DueDate.GetValueOrDefault()),
                EntityType = entityType,
                EntityId = entityId,
                Data = ToJson(payload.Data),
                Priority = payload.Priority,
                Status = requestStatus.Name,
                StatusLabel = requestStatus.Label,
                SourceBy = payload.SourceBy,
                Role = payload.Role,
                Notes = payload.Notes,
                CompletedDate = null
            };

            RequestType requestType;

            if (!payload.IsCustom)
            {
                requestType = Configuration.RequestTypes[entityType]
                    .FirstOrDefault(x => string.Equals(x.RequestId, requestId, StringComparison.CurrentCultureIgnoreCase));
                if (requestType == null)
                    throw new NotFoundException($"{requestId} not found in service configuration, please check");
            }
            else
            {
                //need to find into custom condition and create the request.
                requestType = GetCustomRequest(entityType, entityId, requestId);
            }

            if (requestType.Activity?.Parameters != null)
            {
                requestType.Activity.Parameters = ToJson(requestType.Activity.Parameters);
            }

            metadata.RequestType = requestType;
            //Repository.Add(metadata);
            AddRequiredCondition(metadata, payload, isDefaultCondition, sendToList);
            SendNotification(metadata, payload.SendTo);
            PublishEvent(isDefaultCondition, metadata, entityType);
            //EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.RequestCreated)}", new Events.RequestCreated { Request = metadata });
        }

        /// <summary>
        /// need to add the condition for supplied recipient
        /// </summary>
        /// <param name="requestData">Request data</param>
        /// <param name="payload">Request payload</param>
        /// <param name="isDefaultCondition"></param>
        private void AddRequiredCondition(IRequest requestData, IRequestPayload payload,
            bool isDefaultCondition = false, List<string> sendToList = null)
        {
            if (payload.SendTo != null && payload.SendTo.Count > 0)
            {
                if (isDefaultCondition)
                {
                    requestData.Recipients = payload.SendTo.ToArray();
                    Repository.Add(requestData);
                }
                else
                {
                    if (sendToList != null && sendToList.Count > 0)
                    {
                        requestData.Recipients = sendToList.ToArray();
                        Repository.Add(requestData);
                    }
                    else
                    {
                        var recipientlist = payload.SendTo;
                        var applicantPairs = GetApplicantsPairs(requestData.EntityId);

                        if (recipientlist != null)
                        {
                            var allReceipients = new List<string>();

                            foreach (var r in recipientlist)
                            {
                                var recipients = new List<string>();

                                if (allReceipients.Contains(r))
                                    continue;

                                if (!recipients.Contains(r))
                                {
                                    recipients.Add(r);
                                    allReceipients.Add(r);
                                }

                                if (applicantPairs.ContainsKey(r))
                                {
                                    var spouseApplicantR = applicantPairs[r];

                                    if (!string.IsNullOrEmpty(spouseApplicantR))
                                    {
                                        if (!recipients.Contains(spouseApplicantR) &
                                            recipientlist.Contains(spouseApplicantR))
                                        {
                                            recipients.Add(spouseApplicantR);
                                            allReceipients.Add(spouseApplicantR);
                                        }
                                    }
                                }

                                requestData.Recipients = recipients.ToArray();
                                Repository.Add(requestData);
                            }
                        }
                    }
                }
            }
            else
            {
                if (requestData.RequestType != null)
                    throw new NotFoundException(
                        $"Recipients are not added  for request : {requestData.RequestType.RequestId}");
                else
                    throw new NotFoundException("Recipients are not added or request is not found");
            }
        }

        private void PublishEvent(bool isDefaultCondition, IRequest request, string entityType)
        {
            if (isDefaultCondition)
            {
                if (Configuration.DefaultUserInformation == null)
                    throw new NotFoundException(
                        "default user information not found in service configuration, please check");

                var scope = new[]
                {
                    Configuration.DefaultUserInformation.UserRole
                };
                //  
                var tokenOld = TokenParser.Parse(TokenReader.Read());
                var tenant = tokenOld?.Tenant;
                var expiration = tokenOld?.Expiration;
                var tokenNew = TokenParser.Issue(tenant, Configuration.DefaultUserInformation.Issuer, expiration,
                    Configuration.DefaultUserInformation.UserName, scope);
                var reader = new StaticTokenReader(tokenNew.Value);
                EventHubFactory.Create(reader)
                    .Publish($"{UppercaseFirst(entityType)}{nameof(Events.RequestCreated)}",
                        new Events.RequestCreated { Request = request }).ContinueWith(t =>
                      {
                          Logger.Info("Event published for default condition.");
                      });
            }
            else
            {
                EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.RequestCreated)}",
                    new Events.RequestCreated { Request = request });
            }
        }

        private void SendNotification(IRequest request, IReadOnlyCollection<string> sendTo)
        {
            try
            {      
                if(Configuration.EnableNotificationForCondition)
                {          
                    var notificationNote = Configuration.AddConditionNotificationNote;

                    if (!string.IsNullOrWhiteSpace(notificationNote))
                        notificationNote = notificationNote.FormatWith(request);
                    if (sendTo == null) return;
                    foreach (var userName in sendTo)
                    {
                        Task.Run(async () => await ReminderService.Add(request.EntityType, request.EntityId, userName,
                            new ReminderService.RequestPayload
                            {
                                Description = notificationNote
                            }));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"The method SendNotification raised an error\n: {ex.Message}");
            }
        }

        private RequestType GetCustomRequest(string entityType, string entityId, string requestId)
        {
            var request = CustomConditionRepository.GetById(entityType, entityId, requestId);
            if (request == null)
                throw new NotFoundException($"{requestId} not found in custom condition, please check");

            var requestType = new RequestType
            {
                ConditionCategory = request.ConditionCategory,
                RequestId = request.Id,
                Description = request.Description,
                Title = request.Title
            };

            return requestType;
        }

        public async Task<ICustomCondition> AddCustom(string entityType, string entityId, ICustomRequestPayload payload)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            if (Configuration.RequestTypes == null)
                throw new NotFoundException("Service seems to be not configured, please check");

            EnsureCustomConditionDataIsValid(payload);
            entityType = EnsureEntityTypeIsValid(entityType);

            EnsureFileConfiguration();
            EnsureIsValidFiles(payload.Documents);
            var customCondition = new CustomCondition
            {
                EntityType = entityType,
                EntityId = entityId,
                Title = payload.Title,
                Description = payload.Description,
                ConditionCategory = payload.ConditionCategory,
                Metadata = payload.MetaData,
                CreatedDate = new TimeBucket(TenantTime.Today)
            };

            CustomConditionRepository.Add(customCondition);

            var tags = new List<string> { "Public" };

            foreach (var file in payload.Documents)
            {
                try
                {
                    await DocumentManager.Create
                    (
                        file.Content,
                        entityType,
                        customCondition.Id,
                        file.FileName,
                        payload.MetaData,
                        tags
                    );
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method AddCustom raised an error\n: {ex.Message}");

                    CustomConditionRepository.Remove(customCondition);
                    //need to remove the document.
                    await RemoveDocuments(entityType, customCondition.Id);
                    throw ex;
                }
            }

            return await Task.Run(() => customCondition);
        }

        public async Task DeleteCustom(string entityType, string entityId, string customRequestId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            if (string.IsNullOrWhiteSpace(customRequestId))
                throw new ArgumentException($"{nameof(customRequestId)} is mandatory", nameof(customRequestId));
            entityType = EnsureEntityTypeIsValid(entityType);

            var request = CustomConditionRepository.GetById(entityType, entityId, customRequestId);
            if (request == null)
                throw new NotFoundException(
                    $"Request {entityType}, {entityId} and id {customRequestId} cannot be found");

            CustomConditionRepository.Remove(request);

            //remove document from document manager service
            await RemoveDocuments(entityType, customRequestId);
        }


        private async Task RemoveDocuments(string entityType, string entityId)
        {
            try
            {
                var uploadedDocument = await DocumentManager.GetAll(entityType, entityId);

                if (uploadedDocument != null)
                {
                    await RemoveDocument(entityType, entityId, uploadedDocument);
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"The method removeDocument raised an error\n: {ex.Message}");
            }
        }

        private async Task RemoveDocument(string entityType, string entityId,
            IEnumerable<LendFoundry.DocumentManager.IDocument> uploadedDocument)
        {
            foreach (var document in uploadedDocument)
            {
                await DocumentManager.Delete(entityType, entityId, document.Id);
            }
        }


        public void Delete(string entityType, string entityId, string requestId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            if (string.IsNullOrWhiteSpace(requestId))
                throw new ArgumentException($"{nameof(requestId)} is mandatory", nameof(requestId));

            entityType = EnsureEntityTypeIsValid(entityType);

            var request = Repository.GetById(entityType, entityId, requestId);
            if (request == null)
                throw new NotFoundException($"Request {entityType}, {entityId} and id {requestId} cannot be found");

            Repository.Remove(request);
            EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.RequestDeleted)}",
                new Events.RequestDeleted { Request = request });
        }

        public IEnumerable<IRequest> GetBy(string entityType, string requestId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(requestId))
                throw new ArgumentException($"{nameof(requestId)} is mandatory", nameof(requestId));

            entityType = EnsureEntityTypeIsValid(entityType);
            return CheckJsonDataObjects(Repository.GetManyBy(entityType, requestId));
        }

        public IEnumerable<IRequest> GetByStatus(string entityType, string entityId, string status)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            entityType = EnsureEntityTypeIsValid(entityType);
            return CheckJsonDataObjects(Repository.GetManyByStatus(entityType, entityId, status));
        }

        public IEnumerable<IRequest> GetBy(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            entityType = EnsureEntityTypeIsValid(entityType);
            return CheckJsonDataObjects(Repository.GetManyByEntityType(entityType));
        }

        public IEnumerable<IRequest> GetByStatus(string entityType, string status)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            entityType = EnsureEntityTypeIsValid(entityType);
            return CheckJsonDataObjects(Repository.GetManyByStatus(entityType, status));
        }

        public IRequest GetBy(string entityType, string requestId, string id)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(requestId))
                throw new ArgumentException($"{nameof(requestId)} is mandatory", nameof(requestId));

            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentException($"{nameof(id)} is mandatory", nameof(id));

            entityType = EnsureEntityTypeIsValid(entityType);

            var metadata = Repository
                .All(r => r.EntityType == entityType &&
                          r.RequestType.RequestId == requestId &&
                          r.Id == id)
                .Result
                .FirstOrDefault();
            return metadata != null ? CheckJsonDataObjects(new[] { metadata }).First() : null;
        }

        public IEnumerable<IRequest> GetBy(string entityType, string requestId, string expressionBody,
            IEnumerable<string> expressionValues)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(expressionBody))
                throw new ArgumentException($"{nameof(expressionBody)} is mandatory", nameof(expressionBody));

            if (expressionValues == null)
                throw new ArgumentException($"{nameof(expressionValues)} is mandatory", nameof(expressionValues));

            entityType = EnsureEntityTypeIsValid(entityType);

            IEnumerable<IRequest> requestList;
            if (string.IsNullOrWhiteSpace(requestId))
                requestList = Repository.All(r => r.EntityType == entityType).Result;
            else
                requestList = Repository.All(r => r.EntityType == entityType && r.RequestType.RequestId == requestId)
                    .Result;

            /*
             * Route used to performs filters with expressions
             * GET /{entityType}/{actionType}/filter/{expressionBody}/{*expressionValues}
             *
             * Example of real filter to fetch all applicant entries which names matches w/ michel, edward and paul.
             * GET /merchant/documents/filter/applicant/michael/edward/paul
             */
            var matchedList = (from a in requestList.Where(x => x.Data != null)
                               let b = new
                               {
                                   id = a.Id,
                                   json = JObject.Parse(a.Data.ToString())
                               }
                               let c = b.json.SelectToken(expressionBody)
                               where expressionValues.Contains(((JValue)c).Value.ToString().ToLower())
                               select a).ToList();

            if (matchedList.Any())
                return CheckJsonDataObjects(matchedList);
            else
                throw new NotFoundException("No results found to provided information");
        }

        public IRequestSummary GetSummary(string entityType, string requestId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(requestId))
                throw new ArgumentException($"{nameof(requestId)} is mandatory", nameof(requestId));

            entityType = EnsureEntityTypeIsValid(entityType);

            var list = Repository.All(r => r.EntityType == entityType &&
                                           r.RequestType.RequestId == requestId).Result.ToList();
            return new RequestSummary
            {
                Cancelled = list.Count(r => r.Status == Status.Rejected.ToString()),
                Completed = list.Count(r => r.Status == Status.Completed.ToString()),
                Requested = list.Count(r => r.Status == Status.Requested.ToString())
            };
        }

        public IRequestSummary GetSummary(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            entityType = EnsureEntityTypeIsValid(entityType);

            var list = Repository.All(r => r.EntityType == entityType).Result.ToList();
            return new RequestSummary
            {
                // Cancelled = list.Count(r => r.Status == RequestStatusConstant.Cancelled),
                Completed = list.Count(r => r.Status == Status.Completed.ToString()),
                Requested = list.Count(r => r.Status == Status.Requested.ToString())
            };
        }

        public IEnumerable<IGroupSummary> GetGroupSummary(string entityType, IEnumerable<string> listIds)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            entityType = EnsureEntityTypeIsValid(entityType);

            var requestType = Configuration.RequestTypes[entityType];
            if (requestType == null)
                throw new NotFoundException($"Entity {entityType} not found in service configuration, please check");

            IEnumerable<IRequest> entityItems = null;
            var enumerable = listIds as string[] ?? listIds.ToArray();
            if (enumerable.Any())
            {
                entityItems = Repository.GetManyByEntityType(entityType)
                    .Where(x => x.Status == Status.Requested.ToString() && enumerable.Contains(x.EntityId));
            }

            return from a in requestType
                   group a by a.GroupName
                into b
                   select new GroupSummary
                   {
                       Name = b.Key,
                       Total = entityItems?.Count(e => e.RequestType.GroupName == b.Key) ?? 0
                   };
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByEntity(string entityType, string entityId)
        {
            var requestList =
                CheckJsonDataObjects(await Repository.All(r => r.EntityType == entityType && r.EntityId == entityId));
            return ApplySorting(requestList);
            //return requestList;
        }

        private IEnumerable<IRequest> ApplySorting(IReadOnlyCollection<IRequest> requestList)
        {
            var orderedRequestList = new List<IRequest>();

            var categoryList = Configuration.RequestOrder;
            if (categoryList != null && categoryList.Count > 0)
            {
                var requestCategory = categoryList.OrderBy(x => x.OrderNo);
                foreach (var category in requestCategory)
                {
                    var orderList = requestList.Where(x =>
                        x.RequestType.ConditionCategory != null &&
                        string.Equals(x.RequestType.ConditionCategory, category.Name, StringComparison.CurrentCultureIgnoreCase)).ToList();
                    var subCategory = category.SubCategory.OrderBy(x => x.OrderNo);
                    foreach (var subCat in subCategory)
                    {
                        var subOrderlist = orderList.Where(x =>
                            x.RequestType.ConditionSubCategory != null &&
                            string.Equals(x.RequestType.ConditionSubCategory, subCat.Name, StringComparison.CurrentCultureIgnoreCase)).ToList();

                        orderedRequestList.AddRange(subOrderlist);
                    }

                    // now we have to add condition which are not configured in order or does not have subcategory (means in custom condition).
                    var categoryConditionList = orderedRequestList.Where(x =>
                        x.RequestType.ConditionCategory != null &&
                        string.Equals(x.RequestType.ConditionCategory, category.Name, StringComparison.CurrentCultureIgnoreCase));
                    var remainingCategoryCondition = orderList.Except(categoryConditionList).ToList();
                    orderedRequestList.AddRange(remainingCategoryCondition);
                }

                // now we have to add condition which are not configured in order or does not have category and  subcategory.
                var remainingCondition = requestList.Except(orderedRequestList).ToList();
                orderedRequestList.AddRange(remainingCondition);
            }
            else
            {
                //if request order configuration not found then return request list itself.
                return requestList;
            }

            return orderedRequestList;
        }

        public async Task<IEnumerable<IRequestType>> GetRequestTypes(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            entityType = EnsureEntityTypeIsValid(entityType);

            if (Configuration.RequestTypes == null)
                throw new NotFoundException("Service seems to be not configured, please check");

            if (!Configuration.RequestTypes.TryGetValue(entityType, out var requestTypes))
                throw new NotFoundException($"No configuration found to {entityType}");

            return await Task.Run(() => requestTypes);
        }

        public async Task<List<string>> GetCategories(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            entityType = EnsureEntityTypeIsValid(entityType);

            if (!Configuration.RequestTypes.TryGetValue(entityType, out var requestTypes))
                throw new NotFoundException($"No configuration found to {entityType}");

            var categoryList = requestTypes.GroupBy(x => x.ConditionCategory).Select(x => x.First()).Select(x => x.ConditionCategory).OrderBy(x => x).ToList<string>();

            return await Task.Run(() => categoryList);
        }

        public async Task<IEnumerable<IRequestTemplate>> GetRequestTemplates(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            entityType = EnsureEntityTypeIsValid(entityType);

            if (Configuration.RequestTemplates == null)
                throw new NotFoundException("Service seems to be not configured for templates, please check");

            if (!Configuration.RequestTemplates.TryGetValue(entityType, out var requestTemplates))
                throw new NotFoundException($"No configuration found to {entityType}");

            return await Task.Run(() => requestTemplates);
        }

        public async Task Accept(string entityType, string entityId, IAcceptRequest acceptRequest)
        {
            if (acceptRequest == null)
                throw new ArgumentException($"{nameof(acceptRequest)} is mandatory", nameof(acceptRequest));

            await AcceptOrReject(entityType, entityId, acceptRequest.RequestId, Status.Completed, acceptRequest.Notes);
        }

        public async Task Reject(string entityType, string entityId, IRejectRequest rejectRequest)
        {
            if (rejectRequest == null)
                throw new ArgumentException($"{nameof(rejectRequest)} is mandatory", nameof(rejectRequest));

            if (rejectRequest == null)
                throw new ArgumentException($"{nameof(rejectRequest)} is mandatory", nameof(rejectRequest));

            await AcceptOrReject(entityType, entityId, rejectRequest.RequestId, Status.Rejected, rejectRequest.Notes);
        }


        private void EnsureFileConfiguration()
        {
            if (Configuration.MaxFileSizeInMB == 0)
                throw new NotFoundException("Service seems to be not configured for max file size, please check");

            if (Configuration.MaxFileUploadCount == 0)
                throw new NotFoundException(
                    "Service seems to be not configured for no of file to be uploaded, please check");
        }

        private void EnsureIsValidFiles(IList<DocumentDetails> files)
        {
            if (files.Count > Configuration.MaxFileUploadCount)
                throw new ArgumentException("max no of file upload allowed is " + Configuration.MaxFileUploadCount,
                    nameof(files));

            var maxfilesize = 1024 * 1024 * Configuration.MaxFileSizeInMB; // IN MB

            foreach (var file in files)
            {
                var length = file.Content.Length;
                if (length < 0)
                {
                    throw new ArgumentException("file length must be greater then zero.", nameof(files));
                }

                if (length > maxfilesize)
                {
                    throw new ArgumentException(
                        "file length must be less then " + Configuration.MaxFileSizeInMB + " MB.", nameof(files));
                }

                var extension = Path.GetExtension(file.FileName);

                if (!CheckExtension(extension))
                    throw new ArgumentException(
                        $"{extension} file type is not allowed.Allowed permissible file types are '{Configuration.PermissibleFileTypes}'", nameof(files));
            }
        }

        public async Task SubmitDocumentWithExplanation(string entityType, string entityId, string requestId,
            IList<DocumentDetails> files, object metaData, string explanation)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            if (string.IsNullOrWhiteSpace(requestId))
                throw new ArgumentException($"{nameof(requestId)} is mandatory", nameof(requestId));

            if ((files == null || files.Count <= 0) && string.IsNullOrEmpty(explanation))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(files));


            if (string.IsNullOrEmpty(Configuration.ExplanationTemplateName))
                throw new NotFoundException(
                    "Service seems to be not configured for Explanation Template Name, please check");

            EnsureFileConfiguration();
            EnsureIsValidFiles(files);
            var uploadedDocumentList =
                new List<LendFoundry.DocumentManager.IDocument>();
            var request = Repository.GetById(entityType, entityId, requestId);

            if (request == null)
                throw new NotFoundException($"Request {entityType}, {entityId} and id {requestId} cannot be found");

            var allowedStatuses = new[] { Status.Requested.ToString(), Status.Pending.ToString() };

            if (!allowedStatuses.Contains(request.Status))
            {
                throw new NotFoundException($"document upload does not allowed in {request.Status} status.");
            }

            var requestStatus = GetRequestStatus(entityType, Status.Pending.ToString());
            if (requestStatus == null)
                throw new NotFoundException(
                    $"{Status.Pending.ToString()} status not found in service configuration, please check");
            var tags = GetTags(request);
            ///// Upload document first

            try
            {
                if (files != null)
                    foreach (var file in files)
                    {
                        var uploadedDocument = await DocumentManager.Create
                        (
                            file.Content,
                            entityType,
                            entityId,
                            file.FileName,
                            metaData,
                            tags
                        );

                        uploadedDocumentList.Add(uploadedDocument);
                    }

                var ipaddress = GetClientIp();
                //when called from DCC service that time need to get ipaddress from metadata.
                if (string.IsNullOrEmpty(ipaddress))
                {
                    if (metaData != null)
                    {
                        var objMetaData = JObject.Parse(metaData.ToString());
                        if (objMetaData?["ipAddress"] != null)
                        {
                            ipaddress = objMetaData["ipAddress"].ToString();
                        }
                    }
                }


                var objData = new { data = explanation, IPAddress = ipaddress, date = TenantTime.Now };

                //var fileName = request.RequestType.RequestId + "_Explanation.pdf";
                var documentName = Configuration.ExplanationDocumentName.FormatWith(request);
                var isExplanationDocumentExists = false;
                if (!string.IsNullOrEmpty(explanation))
                {
                    request.Explanation = explanation;

                    //now need to delete the existing document related to explanation and add new one
                    //first get the document list of the condition
                    var existingDocumentList = (await GetDocumentList(entityType, entityId, requestId))?.ToList();
                    LendFoundry.DocumentManager.IDocument explanationDocument = null;
                    if (existingDocumentList != null && existingDocumentList.Any())
                    {
                        explanationDocument = existingDocumentList.FirstOrDefault(x => string.Equals(x.FileName, documentName, StringComparison.CurrentCultureIgnoreCase));
                        if (explanationDocument != null)
                        {
                            isExplanationDocumentExists = true;
                        }
                    }

                    var document = new LendFoundry.DocumentGenerator.Document
                    {
                        Data = objData,
                        Format = DocumentFormat.Pdf,
                        Name = documentName,
                        Version = "1.0"
                    };
                    try
                    {
                        var documentResult = await DocumentGenerator.Generate(Configuration.ExplanationTemplateName, LendFoundry.TemplateManager.Format.Html, document);


                        if (documentResult == null || documentResult.Content.Length == 0)
                            throw new NotFoundException(
                                $"The Document with template name:{Configuration.ExplanationTemplateName} was not found.");

                        using (var stream = new MemoryStream(documentResult.Content))
                        {
                            if (isExplanationDocumentExists)
                            {
                                await DocumentManager.Delete(entityType, entityId, explanationDocument.Id);
                            }

                            var uploadedDocument = await
                                DocumentManager.Create
                                (
                                    stream,
                                    entityType,
                                    entityId,
                                    documentName,
                                    metaData,
                                    tags
                                );


                            uploadedDocumentList.Add(uploadedDocument);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Info("error in method SubmitDocumentWithExplanation Message when generating document:  " + ex.Message);
                    }
                }

                request.Status = requestStatus.Name;
                request.StatusLabel = requestStatus.Label;
                request.Skipped = false;
                request.SubmittedDate = new TimeBucket(TenantTime.Today);
                // update request and trigger proper event
                Repository.Update(request);
                await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.RequestSubmitted)}",
                    new Events.RequestSubmitted { Request = request });
            }
            catch (Exception ex)
            {
                Logger.Info("error in method SubmitDocumentWithExplanation Message :  " + ex.Message);
                await RemoveDocument(entityType, entityId, uploadedDocumentList);
            }
        }

        public async Task<IEnumerable<LendFoundry.DocumentManager.IDocument>> GetDocumentList(string entityType,
            string entityId, string requestId)
        {
            var doclist = await DocumentManager.GetAll(entityType, entityId);
            var li = doclist.ToList();
            var objDocument = new List<LendFoundry.DocumentManager.IDocument>();

            //dynamic metaData =  li.Select(x => x.Metadata).ToList().ForEach(s => JObject.Parse(s.ToString()));
            foreach (var doc in li)
            {
                if (doc.Metadata == null) continue;
                var metaData = JObject.Parse(doc.Metadata.ToString());
                if (metaData?["requestId"] == null) continue;
                var reqid = metaData["requestId"].ToString();
                if (reqid == requestId)
                    objDocument.Add(doc);
            }

            return objDocument;
        }


        public async Task AddDefault(string entityType, string entityId, IRequestPayload payload)
        {           
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            if (Configuration.RequestTypes == null)
                throw new NotFoundException("Service seems to be not configured, please check");

            if (!Configuration.RequestTypes.ContainsKey(entityType))
                throw new NotFoundException($"No configuration found to {entityType}");

            if (string.IsNullOrWhiteSpace(Configuration.DecisionEngineDefaultConditionRuleName))
                throw new NotFoundException("Add default condition rule not found in configuration");

            EnsureDataIsValid(payload);

            if (payload.ApplicationData == null)
                throw new ArgumentException($"{nameof(payload.ApplicationData)} is mandatory", nameof(payload.ApplicationData));
           
            try
            {
                Logger.Info(
                    $"Adding default condition for application # {entityId} With ApplicationData :  {payload.ApplicationData}");

                var defaultConditionResponse = DecisionEngine.Execute<dynamic, DefaultConditionResponse>(
                    Configuration.DecisionEngineDefaultConditionRuleName, new
                    {
                        payload = payload.ApplicationData
                    });

                if (defaultConditionResponse.Data != null)
                {
                    Logger.Info(
                        $"Decision Engine response Result : {defaultConditionResponse.Result} \n response data count : {defaultConditionResponse.Data.Length}");
                }
                else
                {
                    if (defaultConditionResponse.Detail != null)
                    {
                        Logger.Info(
                            $"Decision Engine response Result : {defaultConditionResponse.Result} \n  Decision Engine response details : ");
                        foreach (var err in defaultConditionResponse.Detail)
                        {
                            Logger.Info($"{err}");
                        }
                    }
                }

                if (defaultConditionResponse.Result)
                {
                    if (defaultConditionResponse.Data != null && defaultConditionResponse.Data.Length > 0)
                    {                        
                        var requestIds = defaultConditionResponse.Data;
                        string userName = payload.UserName;
                        await Repository.RemoveManyWithEntityTypeAndEntityIdWithRequestIds(entityType, entityId, requestIds, userName,Configuration.NotAllowToEditOrDeleteRequestIds);
                        IEnumerable<IRequest> conditionsList = null;
                        await Task.Run(() =>
                        {
                            foreach (var requestId in requestIds)
                            {                               
                                Logger.Info($"Adding condition : {requestId}");
                                if (Configuration.CombinedConditionList.Contains(requestId))
                                {
                                    if (conditionsList == null)
                                    {
                                         conditionsList = Repository.GetCombinedListByUser(entityType, entityId, payload.UserName, payload.spouseUserName).Result;
                                        conditionsList = conditionsList.Where(p => Configuration.CombinedConditionList.Contains(p.RequestType.RequestId));
                                    }
                                    if (!conditionsList.Any(p => p.RequestType.RequestId == requestId))
                                    {                                        
                                            Add(entityType, entityId, requestId, payload, true, new List<string> { userName });                                        
                                    }                                    
                                }
                                else
                                {
                                    Add(entityType, entityId, requestId, payload, true);
                                }
                            }
                        });
                    }
                    else
                    {
                        Logger.Error(
                            $"no default condition found for application # {entityId} With ApplicationData {payload.ApplicationData}");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"The method AddDefault raised an error\n: {ex.Message}");
                throw ex;
            }
        }

        private List<string> GetTags(IRequest request)
        {
            var tags = new List<string>();
            try
            {
                if (request.RequestType.Activity.Parameters != null)
                {
                    var obj = JObject.Parse(request.RequestType.Activity.Parameters.ToString());
                    if (obj != null)
                    {
                        var tag = obj["tags"].ToArray();
                        foreach (var o in tag)
                        {
                            tags.Add(o.ToString());
                        }
                    }
                }
            }
            catch
            {
                tags = null;
            }

            return tags;
        }

        private void EnsureCustomConditionDataIsValid(ICustomRequestPayload payload)
        {
            if (payload == null)
                throw new ArgumentException($"{nameof(payload)} is mandatory", nameof(payload));

            if (payload.ConditionCategory == null)
                throw new ArgumentException($"{nameof(payload.ConditionCategory)} is mandatory", nameof(payload.ConditionCategory));

            var conditionCategory = payload.ConditionCategory.ToLower();
            if (Lookup.GetLookupEntry("requiredConditionCategory", conditionCategory) == null)
                throw new ArgumentException($"{conditionCategory} is not a valid condition category.", nameof(payload.ConditionCategory));


            if (string.IsNullOrWhiteSpace(payload.Title))
                throw new ArgumentException($"{nameof(payload.Title)} is mandatory", nameof(payload.Title));

            //if (string.IsNullOrWhiteSpace(payload.Description))
            //    throw new ArgumentException($"{nameof(payload.Description)} is mandatory");
        }

        private void EnsureDataIsValid(IRequestPayload payload)
        {
            if (payload == null)
                throw new ArgumentException($"{nameof(payload)} is mandatory", nameof(payload));

            if (payload.DueDate == null)
                throw new ArgumentException($"{nameof(payload.DueDate)} is mandatory", nameof(payload.DueDate));

            if (payload.DueDate < TenantTime.Today.Date)
                throw new ArgumentException("Due date should be greater than or equal to Current date.", nameof(payload.DueDate));

            if (payload.Priority == RequestPriority.Undefined)
                throw new ArgumentException($"{nameof(payload.Priority)} is mandatory", nameof(payload.Priority));

            if (payload.SendTo == null)
                throw new ArgumentException($"{nameof(payload.SendTo)} is mandatory", nameof(payload.SendTo));

            if (payload.SendTo.Count == 0)
                throw new ArgumentException($"{nameof(payload.SendTo)} is mandatory", nameof(payload.SendTo));
        }

        private string EnsureEntityTypeIsValid(string entityType)
        {
            entityType = entityType.ToLower();
            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity", nameof(entityType));
            return entityType;
        }

        private List<IRequest> CheckJsonDataObjects(IEnumerable<IRequest> requestList)
        {
            return requestList.Select(x =>
            {
                x.Data = FromJson(x.Data);
                if (x.RequestType.Activity != null)
                    x.RequestType.Activity.Parameters = FromJson(x.RequestType.Activity.Parameters);
                return x;
            }).ToList();
        }

        private string ToJson(object obj)
        {
            if (obj is string)
                return obj.ToString();
            return obj != null ? JsonConvert.SerializeObject(obj, Formatting.Indented) : null;
        }

        private object FromJson(object obj)
        {
            return obj != null ? JsonConvert.DeserializeObject<dynamic>(obj.ToString()) : null;
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }

        private bool CheckExtension(string extension)
        {
            extension = extension.Replace(".", "");
            if (Configuration.PermissibleFileTypes == null)
                throw new NotFoundException(
                    "Service seems to be not configured for permissible file types, please check");

            var permissibleFileTypes = Configuration.PermissibleFileTypes;
            var permissible = permissibleFileTypes.Split(',');
            if (permissible.Any())
            {
                var permissibleList = permissible.ToList();
                return permissibleList.Any(x => x.ToLower() == extension.ToLower());
            }

            return false;
        }

        private string GetClientIp()
        {
            var headers = HttpAccessor?.HttpContext?.Request?.Headers;

            if (headers == null)
                return null;

            var key = headers.Keys.FirstOrDefault(k =>
                k.Equals("x-forwarded-for", StringComparison.InvariantCultureIgnoreCase));
            if (key != null)
                return headers[key];

            key = headers.Keys.FirstOrDefault(k =>
                k.Equals("x-client-ip", StringComparison.InvariantCultureIgnoreCase));
            if (key != null)
                return headers[key];

            return null;
        }


        private RequestStatus GetRequestStatus(string entityType, string requestStatus)
        {
            return Configuration.RequestStatuses[entityType]
                .FirstOrDefault(x => x.Name.ToLower() == requestStatus.ToLower());
        }

        private async Task AcceptOrReject(string entityType, string entityId, string requestId, Status status,
            string notes)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));
            entityType = EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(requestId))
                throw new ArgumentException($"{nameof(requestId)} is mandatory", nameof(requestId));

            if (Configuration.RequestStatuses == null)
                throw new NotFoundException("Service seems to be not configured for request status, please check");

            var requestStatus = GetRequestStatus(entityType, status.ToString());
            if (requestStatus == null)
                throw new NotFoundException($"{status} not found in service configuration, please check");

            // get request to be updated
            var request = Repository.GetById(entityType, entityId, requestId);
            if (request == null)
                throw new NotFoundException($"Request {entityType}, {entityId} and id {requestId} cannot be found");
            if (string.Equals(request.Status, Status.Requested.ToString(), StringComparison.CurrentCultureIgnoreCase))
                throw new ArgumentException("Request has not been accepted due to missing document. ");

            if (string.Equals(request.Status, requestStatus.Name, StringComparison.CurrentCultureIgnoreCase))
                throw new ArgumentException($"Request {requestId} has been {requestStatus.Label} already");
            if ((string.Equals(request.Status, Status.Completed.ToString(), StringComparison.CurrentCultureIgnoreCase)) ||
                (string.Equals(request.Status, Status.Rejected.ToString(), StringComparison.CurrentCultureIgnoreCase)))
                throw new ArgumentException($"Request {requestId} has been {request.StatusLabel} already");

            request.Status = requestStatus.Name;
            request.StatusLabel = requestStatus.Label;
            request.Notes = notes;
            switch (status)
            {
                case Status.Completed:
                case Status.Rejected:
                    request.CompletedDate = new TimeBucket(TenantTime.Today);
                    break;
            }

            //request.RejectedDate = new TimeBucket(TenantTime.Today);

            // update request and trigger proper event
            await Repository.ChangeStatus(request.Id, request);

            switch (status)
            {
                case Status.Rejected:
                    {
                        //need to add same condition again with new (requested) status.
                        //need to check this condition is custom or not.
                        if (request != null)
                        {
                            if (request.RequestType != null)
                            {
                                Logger.Debug($"Starting recreating new request for {request.RequestType.RequestId}");
                                bool isCustom = false;
                                try
                                {
                                    var customCondition = GetCustomRequest(entityType, entityId, request.RequestType.RequestId);
                                    if (customCondition != null)
                                        isCustom = true;
                                }
                                catch (Exception ex)
                                {
                                    Logger.Debug($"{request.RequestType.RequestId} is not a custom request."+ ex.Message);
                                }
                                Logger.Debug($"Is Custom Condition for {isCustom}");
                                IRequestPayload requestPayload = new RequestPayload();
                                requestPayload.Priority = request.Priority;
                                requestPayload.DueDate = DateTime.Now;
                                requestPayload.SourceBy = request.SourceBy;
                                requestPayload.Role = request.Role;
                                requestPayload.IsCustom = isCustom;
                                requestPayload.SendTo = request.Recipients.ToList();
                                requestPayload.Notes = request.Notes;
                                requestPayload.Data = request.Data;

                                Add(entityType, entityId, request.RequestType.RequestId, requestPayload);

                                Logger.Debug($"Ending recreating new request for {request.RequestType.RequestId}");
                            }
                        }
                        break;
                    }
            }

            // Send reminder notification
            if(Configuration.EnableNotificationForCondition)
            {
            
                var acceptedOrRejectedNote = "";
                if (!string.IsNullOrWhiteSpace(Configuration.AcceptedOrRejectedNote))
                    acceptedOrRejectedNote = Configuration.AcceptedOrRejectedNote.FormatWith(request);

                var description = acceptedOrRejectedNote;
                if (!string.IsNullOrWhiteSpace(request.Notes))
                    description = $"{description}\n{request.Notes}";

            
                var teamAssignment = await AssignmentService.Get(entityType, entityId);
                foreach (var team in teamAssignment)
                {
                    await ReminderService.Add(entityType, entityId, team.Assignee, new ReminderService.RequestPayload
                    {
                        Description = description
                    });
                }
            }

            switch (status)
            {
                case Status.Completed:
                    await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.RequestCompleted)}",
                        new Events.RequestCompleted { Request = request });
                    break;

                case Status.Rejected:
                    await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.RequestRejected)}",
                        new Events.RequestRejected { Request = request });
                    break;
            }
        }

        public async Task MarkAsSkipped(string entityType, string entityId, string requestId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));
            entityType = EnsureEntityTypeIsValid(entityType);

            // get request to be updated
            var request = Repository.GetById(entityType, entityId, requestId);
            if (request == null)
                throw new NotFoundException($"Request {entityType}, {entityId} and id {requestId} cannot be found");

            await Repository.MarkAsSkipped(requestId, true);
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByUser(string entityType, string entityId)
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            entityType = EnsureEntityTypeIsValid(entityType);


            var requestList = CheckJsonDataObjects(await Repository.GetManyByUser(entityType, entityId, userName));
            return ApplySorting(requestList);
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByUserByStatus(string entityType, string entityId,
            string status)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            entityType = EnsureEntityTypeIsValid(entityType);

            var userName = GetTokenUserName();

            var requestList =
                CheckJsonDataObjects(await Repository.GetManyByUserByStatus(entityType, entityId, status, userName));
            return ApplySorting(requestList);
        }

        public async Task<IEnumerable<IRequest>> GetRequestsByUserWithSpouse(string entityType, string entityId)
        {
            var requestList = await GetBorrowerSpouseRequestList(entityType, entityId, string.Empty);
            return ApplySorting(requestList);
        }


        public async Task<IEnumerable<IRequest>> GetRequestsByUserWithSpouseByStatus(string entityType, string entityId,
            string status)
        {
            var requestList = await GetBorrowerSpouseRequestList(entityType, entityId, status);
            return ApplySorting(requestList);
        }

        private async Task<List<IRequest>> GetBorrowerSpouseRequestList(string entityType, string entityId,
            string status)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            entityType = EnsureEntityTypeIsValid(entityType);


            var userName = GetTokenUserName();
            var spouseUserName = string.Empty;

            var applicantPairs = GetApplicantsPairs(entityId);
            if (applicantPairs.ContainsKey(userName))
            {
                spouseUserName = applicantPairs[userName];
            }

            List<IRequest> requestListAll;
            //now first get the borrower required condition
            if (!string.IsNullOrEmpty(status))
            {
                requestListAll = (await Repository.All(r =>
                    r.EntityType == entityType && r.EntityId == entityId && r.Status.ToLower() == status.ToLower())).ToList();
            }
            else
            {
                requestListAll = (await Repository.All(r => r.EntityType == entityType && r.EntityId == entityId)).ToList();
            }

            var borrowerSpouseRequestList = requestListAll.Where(x => x.Recipients != null && (x.Recipients.Any(y => y.Equals(spouseUserName)) || x.Recipients.Any(y => y.Equals(userName)))).ToList();

            return CheckJsonDataObjects(borrowerSpouseRequestList);
        }

        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }

        private IDictionary<string, string> GetApplicantsPairs(string entityId)
        {
            IDictionary<string, string> applicantPairsList = new Dictionary<string, string>();
            try
            {
                var applicationBorrowerCoBorrowerResponse = DecisionEngine.Execute<dynamic, BorrowerCoBorrowerResponse>(
                    Configuration.DecisionEngineGetBorrowerCoBorrowerRuleName, new { applicationNumber = entityId });
                //we have to combine borrower and spouse into single condition.
                if (applicationBorrowerCoBorrowerResponse?.Data != null)
                {
                    foreach (var applicant in applicationBorrowerCoBorrowerResponse.Data)
                    {
                        if (!applicantPairsList.ContainsKey(applicant.UserName))
                            applicantPairsList.Add(applicant.UserName, applicant.SpouseUserName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Debug($"no borrower ,spouse and co-borrower list found for# {entityId}", ex);
            }

            return applicantPairsList;
        }
        /// <summary>
        /// This function fetches the information of the logged in user
        /// </summary>
        /// <returns></returns>
        private async Task<string> GetCurrentUser()
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                var username = token?.Subject;
                if (!string.IsNullOrEmpty(username)) { username = username.ToLower(); }
                return await Task.Run(() => username);

            }
            catch (InvalidUserException exception)
            {
                throw new InvalidUserException(exception.Message);
            }
        }
        public async Task UpdateInviteIdWithUserName(string entityType, string entityId, string inviteId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory", nameof(entityId));

            if (string.IsNullOrWhiteSpace(inviteId))
                throw new ArgumentException($"{nameof(inviteId)} is mandatory", nameof(inviteId));

            entityType = EnsureEntityTypeIsValid(entityType);
            string username =await  GetCurrentUser();
           var dataResults = await Repository.GetManyByInviteId(entityType, entityId, inviteId);

            if(dataResults != null)
            {               
                foreach (var data in dataResults)
                {
                   List<string> updatedRecipients = new List<string>();
                    foreach (var recipient in data.Recipients)
                    {
                       
                        updatedRecipients.Add(recipient == inviteId ? username : recipient);
                    }
                    data.Recipients = updatedRecipients.ToArray();
                    Repository.Update(data);
                }
                                
            }
        }
    }
}