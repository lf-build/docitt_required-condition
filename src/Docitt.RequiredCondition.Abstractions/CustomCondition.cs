﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace Docitt.RequiredCondition
{
    public class CustomCondition : Aggregate, ICustomCondition
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string ConditionCategory { get; set;}
        public string Description { get; set; }
        public string Title { get; set; }

        public string Metadata { get; set; }
        public TimeBucket CreatedDate { get; set; }
    }
}
