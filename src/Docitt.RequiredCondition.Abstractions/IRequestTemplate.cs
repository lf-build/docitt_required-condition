﻿namespace Docitt.RequiredCondition
{
    public interface IRequestTemplate
    {
        string Description { get; set; }
        string Name { get; set; }
    }
}