﻿namespace Docitt.RequiredCondition.Events
{
    public class RequestRejected
    {
        public IRequest Request { get; set; }
    }
}