﻿namespace Docitt.RequiredCondition.Events
{
    public class RequestSubmitted
    {
        public IRequest Request { get; set; }
    }
}