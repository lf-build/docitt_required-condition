﻿namespace Docitt.RequiredCondition.Events
{
    public class RequestCreated
    {
        public IRequest Request { get; set; }
    }
}