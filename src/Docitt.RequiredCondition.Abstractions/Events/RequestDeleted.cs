﻿namespace Docitt.RequiredCondition.Events
{
    public class RequestDeleted
    {
        public IRequest Request { get; set; }
    }
}