﻿namespace Docitt.RequiredCondition.Events
{
    public class RequestCompleted
    {
        public IRequest Request { get; set; }
    }
}