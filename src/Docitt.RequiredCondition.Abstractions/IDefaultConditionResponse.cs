﻿namespace Docitt.RequiredCondition
{
    public interface IDefaultConditionResponse
    {
        string[] Data { get; set; }
        string[] Detail { get; set; }
        bool Result { get; set; }
    }
}