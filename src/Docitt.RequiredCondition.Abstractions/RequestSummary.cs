﻿namespace Docitt.RequiredCondition
{
    public class RequestSummary : IRequestSummary
    {
        public int Requested { get; set; }
        public int Completed { get; set; }
        public int Cancelled { get; set; }
    }
}