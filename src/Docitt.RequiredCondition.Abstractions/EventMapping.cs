﻿namespace Docitt.RequiredCondition
{
    public class EventMapping
    {
        public string EventName { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
    }
}