﻿namespace Docitt.RequiredCondition
{
    public interface IRequestSummary
    {
        int Requested { get; set; }
        int Completed { get; set; }
        int Cancelled { get; set; }
    }
}