﻿namespace Docitt.RequiredCondition
{
    public class RequestStatus
    {
        public string Name { get; set; }
        public string Label { get; set; }
    }
}