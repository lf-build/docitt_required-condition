﻿namespace Docitt.RequiredCondition
{
    public class AcceptRequest : IAcceptRequest
    {
        public string Notes { get; set; }

        public string RequestId { get; set; }
    }
}