﻿using System.IO;

namespace Docitt.RequiredCondition
{
    public class DocumentDetails
    {
        public string FileName { get; set; }
        public Stream Content { get; set; }
    }
}