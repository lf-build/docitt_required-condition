﻿using System;
using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public interface IRequestType
    {
        string GroupName { get; set; }
        string[] TemplateNames { get; set; }
        string MasterType { get; set; }
        string ConditionCategory { get; set; }
        string RequestId { get; set; }
        string Description { get; set; }
        string Title { get; set; }
        string EventName { get; set; }

        Activity Activity { get; set; }
    }
}