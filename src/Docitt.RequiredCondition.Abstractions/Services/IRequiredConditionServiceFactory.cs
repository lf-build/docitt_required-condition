﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.RequiredCondition
{
    public interface IRequiredConditionServiceFactory
    {
        IRequiredConditionService Create(StaticTokenReader reader, ILogger logger);
    }
}