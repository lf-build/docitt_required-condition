﻿using LendFoundry.Security.Tokens;

namespace Docitt.RequiredCondition
{
    public interface IRequiredConditionRepositoryFactory
    {
        IRequiredConditionRepository Create(ITokenReader reader);
    }
}