﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public interface ICustomConditionRepository : IRepository<ICustomCondition>
    {
        ICustomCondition GetById(string entityType, string entityId, string id);
        IEnumerable<ICustomCondition> GetManyBy(string entityType, string requestId);
        IEnumerable<ICustomCondition> GetManyByEntityType(string entityType);
    }
}