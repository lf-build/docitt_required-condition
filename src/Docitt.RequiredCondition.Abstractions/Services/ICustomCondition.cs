﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
namespace Docitt.RequiredCondition
{
    public interface ICustomCondition : IAggregate
    {
        string ConditionCategory { get; set; }
        string Description { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        TimeBucket CreatedDate { get; set; }

        string Metadata { get; set; }
        string Title { get; set; }
    }
}