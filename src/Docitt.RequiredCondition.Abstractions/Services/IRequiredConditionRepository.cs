﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition

{
    public interface IRequiredConditionRepository : IRepository<IRequest>
    {
        IEnumerable<IRequest> GetManyBy(string eventName);

        IEnumerable<IRequest> GetManyByEntityType(string entityType);

        IRequest GetById(string entityType, string entityId, string requestId);

        IEnumerable<IRequest> GetManyBy(string entityType, string requestId);

        IEnumerable<IRequest> GetManyBy(string entityType, Status status);

        IEnumerable<IRequest> GetManyByStatus(string entityType, string entityId, string status);

        IEnumerable<IRequest> GetManyByStatus(string entityType, string status);

        IRequest GetSubmittedById(string entityType, string entityId, string requestId);

        Task ChangeStatus(string requestId, IRequest request);       

        Task MarkAsSkipped(string requestId, bool skipped);

        Task<IEnumerable<IRequest>> GetManyByUser(string entityType, string entityId,string userName);
        Task<IEnumerable<IRequest>> GetManyByUserByStatus(string entityType, string entityId, string status,string userName);

        Task RemoveManyWithEntityTypeAndEntityIdWithRequestIds(string entityType, string entityId, string[] requestIds, string userName, List<string> notAllowToEditOrDeleteRequestIds);

        Task<IEnumerable<IRequest>> GetManyByInviteId(string entityType, string entityId, string inviteId);

        Task<IEnumerable<IRequest>> GetCombinedListByUser(string entityType, string entityId, string userName, string spouseUserName);
    }
}