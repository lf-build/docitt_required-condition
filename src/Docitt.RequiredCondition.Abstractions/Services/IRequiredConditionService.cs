﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition
{
    public interface IRequiredConditionService
    {
        void Add(string entityType, string entityId, string requestId, IRequestPayload request,bool isDefaultCondition = false, List<string> sendToList = null);

        void Delete(string entityType, string entityId, string requestId);

        IEnumerable<IRequest> GetBy(string entityType, string requestId);

        IEnumerable<IRequest> GetByStatus(string entityType, string entityId, string status);

        IEnumerable<IRequest> GetBy(string entityType);

        IEnumerable<IRequest> GetByStatus(string entityType, string status);

        IRequest GetBy(string entityType, string requestId, string id);

        IEnumerable<IRequest> GetBy(string entityType, string requestId, string expressionBody, IEnumerable<string> expressionValues);

        IRequestSummary GetSummary(string entityType, string requestId);

        IRequestSummary GetSummary(string entityType);

        IEnumerable<IGroupSummary> GetGroupSummary(string entityType, IEnumerable<string> entityIds);

        Task<IEnumerable<IRequest>> GetRequestsByEntity(string entityType, string entityId);


        Task<IEnumerable<IRequestType>> GetRequestTypes(string entityType);
        Task SubmitDocumentWithExplanation(string entityType, string entityId, string requestId, IList<DocumentDetails> files, object metaData, string explanatation);

        Task<IEnumerable<LendFoundry.DocumentManager.IDocument>> GetDocumentList(string entityType, string entityId, string requestId);

        Task AddDefault(string entityType, string entityId, IRequestPayload payload);

        Task Accept(string entityType, string entityId, IAcceptRequest acceptRequest);

        Task Reject(string entityType, string entityId, IRejectRequest rejectRequest);

        Task<IEnumerable<IRequestTemplate>> GetRequestTemplates(string entityType);
        Task<ICustomCondition> AddCustom(string entityType, string entityId, ICustomRequestPayload payload);

        Task DeleteCustom(string entityType, string entityId, string customRequestId);

        Task MarkAsSkipped(string entityType, string entityId, string requestId);

        Task<List<string>> GetCategories(string entityType);
        Task<IEnumerable<IRequest>> GetRequestsByUser(string entityType, string entityId);
        Task<IEnumerable<IRequest>> GetRequestsByUserByStatus(string entityType, string entityId,string status);
        Task<IEnumerable<IRequest>> GetRequestsByUserWithSpouse(string entityType, string entityId);
       Task<IEnumerable<IRequest>> GetRequestsByUserWithSpouseByStatus(string entityType, string entityId,string status);
       Task UpdateInviteIdWithUserName(string entityType, string entityId, string inviteId);


    }
}