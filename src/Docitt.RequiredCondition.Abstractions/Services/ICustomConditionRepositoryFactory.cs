﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition
{
    public interface ICustomConditionRepositoryFactory
    {
        ICustomConditionRepository Create(ITokenReader reader);
    }
}
