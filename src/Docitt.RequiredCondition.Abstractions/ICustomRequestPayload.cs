﻿using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public interface ICustomRequestPayload
    {
        string ConditionCategory { get; set; }
        string Description { get; set; }
        IList<DocumentDetails> Documents { get; set; }
        string MetaData { get; set; }
        string Title { get; set; }
    }
}