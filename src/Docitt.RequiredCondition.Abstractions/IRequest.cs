﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace Docitt.RequiredCondition
{
    public interface IRequest : IAggregate
    {
        TimeBucket SubmittedDate { get; set; }
        RequestPriority Priority { get; set; }
        object Data { get; set; }
        TimeBucket DueDate { get; set; }
        string EntityType { get; set; }
        string EntityId { get; set; }
        string Status { get; set; }

        string StatusLabel { get; set; }
        RequestType RequestType { get; set; }

        string Notes { get; set; }
        TimeBucket CompletedDate { get; set; }
        string SourceBy { get; set; }
        string Role { get; set; }
        TimeBucket RequestedDate { get; set; }
        bool Skipped { get; set; }

        string[] Recipients { get; set; }
        string  Explanation {get; set;}

    }
}