﻿using System;
using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public interface IRequestPayload
    {
        object Data { get; set; }
        DateTime? DueDate { get; set; }
        string EntityId { get; set; }
        bool IsCustom { get; set; }
        string Notes { get; set; }
        RequestPriority Priority { get; set; }
        string Role { get; set; }
        List<string> SendTo { get; set; }
        string SourceBy { get; set; }        
        object ApplicationData { get; set; }
        bool isSpouseUser { get; set; }
        string spouseUserName { get; set; }
        string UserName { get; set; }
    }
}