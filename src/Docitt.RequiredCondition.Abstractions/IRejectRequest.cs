﻿namespace Docitt.RequiredCondition
{
    public interface IRejectRequest
    {
        string RequestId { get; set; }
        string Notes { get; set; }
    }
}