﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition
{
    public class DefaultUser
    {
        public string UserName { get; set; }
        public string UserRole { get; set; }

        public string Issuer { get; set; }
    }
}
