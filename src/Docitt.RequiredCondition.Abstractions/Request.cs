﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace Docitt.RequiredCondition
{
    public class Request : Aggregate, IRequest
    {
        public TimeBucket SubmittedDate { get; set; }
        public RequestPriority Priority { get; set; }
        public object Data { get; set; }
        public TimeBucket DueDate { get; set; }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string Status { get; set; }
        public string StatusLabel { get; set; }
        public RequestType RequestType { get; set; }

        public string Notes { get; set; }
        public TimeBucket CompletedDate { get; set; }

        public string SourceBy { get; set; }
        public string Role { get; set; }
        public TimeBucket RequestedDate { get; set; }

        public bool Skipped { get; set; }

        public  string[] Recipients { get; set; }

        public string  Explanation {get; set;}
    }
}