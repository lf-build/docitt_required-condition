using Docitt.Questionnaire;
using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public class BorrowerCoBorrowerResponse
    {
        public string Status { get; set; }

        public List<BorrowerCoBorrowerSpouseInfo> Data { get; set; }

        public Error error {get;set;}
    }


    public class Error{
        public string code {get;set;}
        public string message {get;set;}
        
    }
}