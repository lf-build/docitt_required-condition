﻿namespace Docitt.RequiredCondition
{
    public class Activity : IActivity
    {
        public string Url { get; set; }
        public object Parameters { get; set; }
    }
}