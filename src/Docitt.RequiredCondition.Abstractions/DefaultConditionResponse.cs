﻿namespace Docitt.RequiredCondition
{
    public class DefaultConditionResponse : IDefaultConditionResponse
    {
        public bool Result { get; set; }
        public string[] Data { get; set; }
        public string[] Detail { get; set; }
    }
}
