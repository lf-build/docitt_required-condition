﻿namespace Docitt.RequiredCondition
{
    public interface IActivity
    {
        string Url { get; set; }
        object Parameters { get; set; }
    }
}