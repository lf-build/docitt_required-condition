﻿using System;
using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public class RequestType : IRequestType
    {
        public string GroupName { get; set; }
        public string MasterType { get; set; }
        public string ConditionCategory { get; set; }

        public string ConditionSubCategory { get; set; }
        public string RequestId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string EventName { get; set; }

        public Activity Activity { get; set; }
        public bool IsDefault { get; set; }

        public bool IsSyncRequired { get; set; }
        public string[] TemplateNames { get; set; }
        public bool IsExplanation { get; set; }

        public bool IsUpload { get; set; }

    }
}