﻿namespace Docitt.RequiredCondition
{
    public enum Status
    {
        Requested,
        Pending,
        Completed,
        Rejected
    }
}