﻿using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public class SortingCategory
    {
        public string Name { get; set; }
        public int OrderNo { get; set; }
        public List<SortingCategory> SubCategory { get; set; }
    }
}
