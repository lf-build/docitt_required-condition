﻿namespace Docitt.RequiredCondition
{
    public interface IAcceptRequest
    {
        string RequestId { get; set; }
        string Notes { get; set; }
    }
}