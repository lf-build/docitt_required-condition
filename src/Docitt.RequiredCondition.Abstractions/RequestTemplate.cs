﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition
{
    public class RequestTemplate : IRequestTemplate
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
