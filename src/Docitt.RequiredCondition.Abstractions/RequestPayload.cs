﻿using System;
using System.Collections.Generic;

namespace Docitt.RequiredCondition
{
    public class RequestPayload : IRequestPayload
    {
        public RequestPriority Priority { get; set; }
        public object Data { get; set; }
        public DateTime? DueDate { get; set; }
        public string EntityId { get; set; }
        public string SourceBy { get; set; }
        public string Role { get; set; }
        public string Notes { get; set; }
        public bool IsCustom { get; set; }
        public List<string> SendTo { get; set; }

        public object ApplicationData { get; set; }
        public bool isSpouseUser { get; set; }
        public string spouseUserName { get; set; }
        public string UserName { get; set; }
    }
}