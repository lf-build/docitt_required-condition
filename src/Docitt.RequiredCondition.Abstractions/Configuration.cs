﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace Docitt.RequiredCondition
{
    public class Configuration : IDependencyConfiguration
    {
        public Dictionary<string, IEnumerable<RequestType>> RequestTypes { get; set; }
        public List<EventMapping> EventMappings { get; set; }
        public Dictionary<string, IEnumerable<RequestStatus>> RequestStatuses { get; set; }
        public Dictionary<string, IEnumerable<RequestTemplate>> RequestTemplates { get; set; }
        public int MaxFileSizeInMB { get; set; }
        public int MaxFileUploadCount { get; set; }

        public string ExplanationTemplateName { get; set; }
        public string ExplanationTemplateVersion { get; set; }

        public string ExplanationDocumentName { get; set; }

        public string PermissibleFileTypes { get; set; }

        public string AcceptedOrRejectedNote { get; set; }

        public string AddConditionNotificationNote { get; set; }

        public string DecisionEngineDefaultConditionRuleName { get; set; }

        public DefaultUser DefaultUserInformation { get; set; }

        public List<SortingCategory> RequestOrder { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string DecisionEngineGetBorrowerCoBorrowerRuleName { get; set; }
        public List<string> CombinedConditionList { get; set; }

        public List<string> NotAllowToEditOrDeleteRequestIds { get; set; }

        public bool EnableNotificationForCondition {get ;set;}
    }
}