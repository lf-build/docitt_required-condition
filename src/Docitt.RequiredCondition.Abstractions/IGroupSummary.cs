﻿namespace Docitt.RequiredCondition
{
    public interface IGroupSummary
    {
        string Name { get; set; }
        int Total { get; set; }
    }
}