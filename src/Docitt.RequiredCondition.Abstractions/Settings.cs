
using System;

namespace Docitt.RequiredCondition
{
    public static class Settings
    {      

        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "required-condition";

    }
}