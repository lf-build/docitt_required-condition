﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition
{
    public class CustomRequestPayload : ICustomRequestPayload
    {
        public string ConditionCategory { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }

        public string MetaData { get; set; }
        public IList<DocumentDetails> Documents { get; set; }
    }
}
