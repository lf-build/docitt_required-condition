﻿namespace Docitt.RequiredCondition
{
    public class GroupSummary : IGroupSummary
    {
        public string Name { get; set; }
        public int Total { get; set; }
    }
}