﻿namespace Docitt.RequiredCondition
{
    public class RejectRequest : IRejectRequest
    {
        public string RequestId { get; set; }
        public string Notes { get; set; }
    }
}