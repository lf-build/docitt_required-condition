﻿namespace Docitt.RequiredCondition
{
    public enum RequestPriority
    {
        Undefined,
        Low,
        Normal,
        High
    }
}