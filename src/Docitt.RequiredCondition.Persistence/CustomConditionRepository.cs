﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;


namespace Docitt.RequiredCondition.Persistence
{
    public class CustomConditionRepository : MongoRepository<ICustomCondition, CustomCondition>,ICustomConditionRepository
    {
        public CustomConditionRepository(ITenantService tenantService, IMongoConfiguration configuration) :
            base(tenantService, configuration, "custom-condition")
        {
            CreateIndexIfNotExists("entity-type", Builders<ICustomCondition>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entity-type-id", Builders<ICustomCondition>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }

        public IEnumerable<ICustomCondition> GetManyBy(string entityType, string requestId)
        {
            return All(r => r.EntityType == entityType && r.Id == requestId).Result;
        }
        public IEnumerable<ICustomCondition> GetManyByEntityType(string entityType)
        {
            return Query.Where(r => r.EntityType == entityType);
        }

        public ICustomCondition GetById(string entityType, string entityId, string id)
        {
            return FindRequestsByEntity(entityType, entityId).FirstOrDefault(r => r.Id == id);
        }

        /// <summary>
        /// Get all requests for given entity
        /// </summary>
        /// <param name="entityType">The type of the entity (e.g.: Application or Loan)</param>
        /// <param name="entityId">The ID of given entity</param>
        /// <returns></returns>
        private IQueryable<ICustomCondition> FindRequestsByEntity(string entityType, string entityId)
        {
            return Query.Where(r => r.EntityType == entityType && r.EntityId == entityId);
        }
    }
}
