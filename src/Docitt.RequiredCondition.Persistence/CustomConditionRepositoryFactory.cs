﻿using Microsoft.Extensions.DependencyInjection;
using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;

namespace Docitt.RequiredCondition.Persistence
{
    public class CustomConditionRepositoryFactory : ICustomConditionRepositoryFactory
    {
        public CustomConditionRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ICustomConditionRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new CustomConditionRepository(tenantService, mongoConfiguration);
        }
    }
}
