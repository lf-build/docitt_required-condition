﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.RequiredCondition.Persistence
{
    public class RequiredConditionRepository : MongoRepository<IRequest, Request>, IRequiredConditionRepository
    {
        static RequiredConditionRepository()
        {
            BsonClassMap.RegisterClassMap<Activity>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Parameters).SetSerializer(new ObjectSerializer());
                var type = typeof(Activity);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<RequestType>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.RequestId);
                var type = typeof(RequestType);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public RequiredConditionRepository(ITenantService tenantService, IMongoConfiguration configuration) :
            base(tenantService, configuration, "required-condition")
        {
            CreateIndexIfNotExists("entity-type", Builders<IRequest>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entity-type-id", Builders<IRequest>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
            CreateIndexIfNotExists("status", Builders<IRequest>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Status));
        }

        public IEnumerable<IRequest> GetManyBy(string entityType, string requestId)
        {
            return All(r => r.EntityType == entityType && r.RequestType.RequestId == requestId).Result;
        }

        public IEnumerable<IRequest> GetManyBy(string entityType, Status status)
        {
            return Query.Where(r => r.EntityType == entityType && r.Status == status.ToString());
        }

        public IEnumerable<IRequest> GetManyByStatus(string entityType, string entityId, string status)
        {
            return FindRequestsByEntity(entityType, entityId).Where(r => r.Status.ToLower() == status);
        }

        public IEnumerable<IRequest> GetManyBy(string eventName)
        {
            return Query.Where(r => r.RequestType.EventName == eventName);
        }

        public IEnumerable<IRequest> GetManyByEntityType(string entityType)
        {
            return Query.Where(r => r.EntityType == entityType);
        }

        public IEnumerable<IRequest> GetManyByStatus(string entityType, string status)
        {
            return Query.Where(r => r.EntityType == entityType && r.Status.ToLower() == status);
        }

        public IRequest GetById(string entityType, string entityId, string id)
        {
            return FindRequestsByEntity(entityType, entityId).FirstOrDefault(r => r.Id == id);
        }

        public IRequest GetSubmittedById(string entityType, string entityId, string id)
        {
            return FindRequestsByEntity(entityType, entityId)
                .FirstOrDefault(r => r.Id == id && r.Status == Status.Pending.ToString());
        }

        /// <summary>
        /// Get all requests for given entity
        /// </summary>
        /// <param name="entityType">The type of the entity (e.g.: Application or Loan)</param>
        /// <param name="entityId">The ID of given entity</param>
        /// <returns></returns>
        private IQueryable<IRequest> FindRequestsByEntity(string entityType, string entityId)
        {
            return Query.Where(r => r.EntityType == entityType && r.EntityId == entityId);
        }

        public async Task ChangeStatus(string requestId, IRequest request)
        {
            await Collection.UpdateOneAsync
            (

                Builders<IRequest>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.Id == requestId),
                Builders<IRequest>.Update.Set(a => a.Status, request.Status)
                    .Set(a => a.StatusLabel, request.StatusLabel)
                    .Set(a => a.CompletedDate, request.CompletedDate)
                      .Set(a => a.Notes, request.Notes)
                    );
        }

        public async Task MarkAsSkipped(string requestId, bool skipped)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IRequest>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.Id == requestId),
                Builders<IRequest>.Update.Set(a => a.Skipped, skipped)
              );
        }
        public async Task<IEnumerable<IRequest>> GetCombinedListByUser(string entityType, string entityId, string userName, string spouseUserName)
        {
            return await Query.Where(r => r.EntityType == entityType && r.EntityId == entityId && r.Recipients.Any(x => x.Equals(userName) ||  x.Equals(spouseUserName))).ToListAsync();
        }
        public async Task<IEnumerable<IRequest>> GetManyByUser(string entityType, string entityId, string userName)
        {
            return await Query.Where(r => r.EntityType == entityType && r.EntityId == entityId && r.Recipients.Any(x => x.Equals(userName))).ToListAsync();
        }
        public async Task<IEnumerable<IRequest>> GetManyByUserByStatus(string entityType, string entityId, string status, string userName)
        {
            return await Query.Where(r => r.EntityType == entityType && r.EntityId == entityId && r.Recipients.Any(x => x.Equals(userName)) && r.Status.ToLower() == status.ToLower()).ToListAsync();
        }

        public async Task RemoveManyWithEntityTypeAndEntityIdWithRequestIds(string entityType, string entityId, string[] requestIds, string userName, List<string> notAllowToEditOrDeleteRequestIds)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IRequest>.Filter;
            var filterNotInRequestIds = builders.Where(db => 
            db.TenantId == tenantId &&
            db.EntityType == entityType &&
            db.EntityId == entityId &&
            db.Status.ToLower() == Status.Requested.ToString().ToLower() && 
            db.RequestType != null && 
            (notAllowToEditOrDeleteRequestIds == null || (notAllowToEditOrDeleteRequestIds!=null && !notAllowToEditOrDeleteRequestIds.Contains(db.RequestType.RequestId) ) )
            &&
            (
                db.Recipients != null && db.Recipients.Contains(userName)                           
                
            ) // Delete all the requests which are not match with current requestIds for specific user. 
             || 
            (
                requestIds.Contains(db.RequestType.RequestId) && 
                db.RequestType.MasterType.ToLower() == MasterType.Transaction.ToString().ToLower()
            ) // Delete all the requests which are match with current requestIds for master type as Transaction. 
            );
            await Collection.DeleteManyAsync(filterNotInRequestIds);
        }

        public async Task<IEnumerable<IRequest>> GetManyByInviteId(string entityType, string entityId, string inviteId)
        {
            return await Query.Where(r => r.EntityType == entityType && r.EntityId == entityId && r.Recipients.Any(x => x.Equals(inviteId))).ToListAsync();
        }      
    }
}