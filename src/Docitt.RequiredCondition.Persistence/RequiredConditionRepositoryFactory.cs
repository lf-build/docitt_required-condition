﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Docitt.RequiredCondition.Persistence
{
    public class RequiredConditionRepositoryFactory : IRequiredConditionRepositoryFactory
    {
        public RequiredConditionRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IRequiredConditionRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new RequiredConditionRepository(tenantService, mongoConfiguration);
        }
    }
}