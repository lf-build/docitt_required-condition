﻿using Docitt.AssignmentEngine.Client;
using Docitt.ReminderService.Client;
using Docitt.RequiredCondition.Persistence;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Foundation.Services;

namespace Docitt.RequiredCondition.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittRequiredConddition"
                });
                 c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
               {
                   Type = "apiKey",
                   Name = "Authorization",
                   Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                   In = "header"
               });
               c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.RequiredCondition.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#endif

            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<Configuration>( Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddDecisionEngine();
            services.AddLookupService();
            // document-generator
            services.AddDocumentGenerator();
            // document-manager
            services.AddDocumentManager();

            // reminder
            services.AddReminderService();
            services.AddAssignmentService();
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            services.AddTransient(p => p.GetService<IConfigurationService<Configuration>>().Get());
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IRequiredConditionRepositoryFactory, RequiredConditionRepositoryFactory>();
            services.AddTransient<IRequiredConditionRepository, RequiredConditionRepository>();
            services.AddTransient<IRequiredConditionServiceFactory, RequiredConditionServiceFactory>();
            services.AddTransient<IRequiredConditionService, RequiredConditionService>();

            services.AddTransient<ICustomConditionRepositoryFactory, CustomConditionRepositoryFactory>();
            services.AddTransient<ICustomConditionRepository, CustomConditionRepository>();
        
            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions((options) =>
            {
                options
                    .AddInterfaceConverter<IActivity, Activity>()
                    .AddInterfaceConverter<IRequest, Request>()
                    .AddInterfaceConverter<IRequestType, RequestType>()
                    .AddInterfaceConverter<IRequestSummary, RequestSummary>()
                    .AddInterfaceConverter<ICustomCondition, CustomCondition>();
            });
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT Required Condition Service");
            });

#endif  
            
            app.UseErrorHandling();
            app.UseRequestLogging();            
            app.UseMvc();
             app.UseConfigurationCacheDependency();
        }
    }
}