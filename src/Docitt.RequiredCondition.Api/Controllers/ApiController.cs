﻿using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.DocumentManager;

namespace Docitt.RequiredCondition.Api.Controllers
{

    /// <summary>
    /// Represents Api controller class.
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="service"></param>
        public ApiController(IRequiredConditionService service)
        {
            Service = service;
        }

        private IRequiredConditionService Service { get; }


        /// <summary>
        /// Create new Condition
        /// </summary>
        /// <param name="entityType">entity type</param>
        /// <param name="entityId">entity id</param>
        /// <param name="requestId">request id</param>
        /// <param name="request">request payload</param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/{requestId}/")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult Add(string entityType, string entityId, string requestId, [FromBody]RequestPayload request)
        {
            try
            {
                return Execute(() => { Service.Add(entityType, entityId, requestId, request); return NoContent(); });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }    
        }

        /// <summary>
        ///  Create new condition with default condition.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        //Create Default Condition
        [HttpPut("/{entityType}/{entityId}/")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddDefault(string entityType, string entityId, [FromBody]RequestPayload request)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await Service.AddDefault(entityType, entityId, request);
                    return NoContent();
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// Get conditions with specific entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        //Get Conditions for entityId
        [HttpGet("/{entityType}/{entityId}")]

        [ProducesResponseType(typeof(IEnumerable<IRequest>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRequestsByEntity(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                var requests = await Service.GetRequestsByEntity(entityType, entityId);
                return Ok(requests);
            });
        }

        /// <summary>
        /// Get request list with specific status.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        //Get Conditions for entityId by status
        [HttpGet("/{entityType}/{entityId}/status/{status}")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetRequestListByStatus(string entityType, string entityId, string status)
        {
            return Execute(() => Ok(Service.GetByStatus(entityType, entityId, status)));
        }

        
        /// <summary>
        /// Get request list with specific entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        //Get Conditions of entityType
        [HttpGet("/{entityType}/")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetRequestList(string entityType)
        {
            return Execute(() => Ok(Service.GetBy(entityType)));
             
        }

        /// <summary>
        /// Get request lists with entity type and status.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        //Get Conditions of entityType by status
        [HttpGet("/{entityType}/status/{status}")]

        [ProducesResponseType(typeof(IEnumerable<IRequest>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetRequestList(string entityType, string status)
        {
            return Execute(() => Ok(Service.GetByStatus(entityType, status)));
        }

        /// <summary>
        /// Get request list with specific entity type, request id and other params.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="requestId"></param>
        /// <param name="expressionBody"></param>
        /// <param name="expressionValues"></param>
        /// <returns></returns>
        //not in use
        [HttpGet("/{entityType}/{requestId}/filter/{expressionBody}/{*expressionValues}")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetRequestList(string entityType, string requestId, string expressionBody, string expressionValues)
        {
            return Execute(() =>
            {
                var expValues = expressionValues.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Select(s => WebUtility.UrlDecode(s).ToLower());
                return Ok(Service.GetBy(entityType, requestId, expressionBody, expValues));
            });
        }

        /// <summary>
        /// Get request lists with specific entity type and other params.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="expressionBody"></param>
        /// <param name="expressionValues"></param>
        /// <returns></returns>
        //not in use
        [HttpGet("/{entityType}/filter/{expressionBody}/{*expressionValues}")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetRequestList(string entityType, string expressionBody, string expressionValues)
        {
            return Execute(() =>
            {
                var expValues = expressionValues.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Select(s => WebUtility.UrlDecode(s).ToLower());
                return Ok(Service.GetBy(entityType, string.Empty, expressionBody, expValues));
            });
        }

        /// <summary>
        /// Get summary with specific entity type and request id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        //not in use
        [HttpGet("/{entityType}/{requestId}/count")]
        [ProducesResponseType(typeof(IRequestSummary), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetSummary(string entityType, string requestId)
        {
            return Execute(() => Ok(Service.GetSummary(entityType, requestId)));
        }

        /// <summary>
        /// Get summary count with specific entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        //not in use
        [HttpGet("/{entityType}/count")]
        [ProducesResponseType(typeof(IRequestSummary), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetSummary(string entityType)
        {
            return Execute(() => Ok(Service.GetSummary(entityType)));
        }

        /// <summary>
        ///  Get request with specific entity type, request id and id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="requestId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        //not in use
        [HttpGet("/{entityType}/{requestId}/{id}/")]
        [ProducesResponseType(typeof(IRequest), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetRequest(string entityType, string requestId, string id)
        {
            return Execute(() => Ok(Service.GetBy(entityType, requestId, id)));
        }

        /// <summary>
        /// Get group summary with specific entity type and ids.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityIds"></param>
        /// <returns></returns>
        //not in use
        [HttpGet("/{entityType}/groups/{*entityIds}/")]
        [ProducesResponseType(typeof(IEnumerable<IGroupSummary>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetGroupSummary(string entityType, string entityIds)
        {
            return Execute(() =>
            {
                var listIds = default(IEnumerable<string>);
                if (entityIds != null)
                    listIds = entityIds.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Select(s => WebUtility.UrlDecode(s));
                return Ok(Service.GetGroupSummary(entityType, listIds));
            });
        }

        /// <summary>
        /// Get request types with specific entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/request-types")]
        [ProducesResponseType(typeof(IEnumerable<IRequestType>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRequestTypes(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                var requestTypes = await Service.GetRequestTypes(entityType);
                return Ok(requestTypes);
            });
        }

         /// <summary>
        /// Get request types with specific entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/categories")]
        [ProducesResponseType(typeof(IEnumerable<string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetCategories(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                var requestTypes = await Service.GetCategories(entityType);
                return Ok(requestTypes);
            });
        }

        /// <summary>
        /// Get request templates with specific entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/request-templates")]
        [ProducesResponseType(typeof(IEnumerable<IRequestTemplate>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRequestTemplates(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
               var requestTemplate = await Service.GetRequestTemplates(entityType);
                return Ok(requestTemplate);
            });
        }

        /// <summary>
        /// Accept contition with specific entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="acceptRequest"></param>
        /// <returns></returns>
        [HttpPut("/{entityType}/{entityId}/accept")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Accept(string entityType, string entityId, [FromBody] AcceptRequest acceptRequest)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await Service.Accept(entityType, entityId, acceptRequest);
                    return NoContent();
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// Reject condition with specific entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="rejectRequest"></param>
        /// <returns></returns>
        [HttpPut("/{entityType}/{entityId}/reject")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Reject(string entityType, string entityId, [FromBody] RejectRequest rejectRequest)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await Service.Reject(entityType, entityId, rejectRequest);
                    return NoContent();
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }    
        }

        /// <summary>
        /// Submit document with explanation for specific entity type, entity id and request id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="requestId"></param>
        /// <param name="files"></param>
        /// <param name="metadata"></param>
        /// <param name="explanation"></param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/{requestId}/submit")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SubmitDocumentWithExplanation(string entityType, string entityId, string requestId, IFormFile[] files, string metadata, string explanation)
        {      

            try
            {
                IList<DocumentDetails> documents = new List<DocumentDetails>();
                object metadataJson = null;
                if (!string.IsNullOrWhiteSpace(metadata))
                    metadataJson = JsonConvert.DeserializeObject<object>(metadata);

                foreach (var file in files)
                {
                    MemoryStream ms = new MemoryStream();
                    file.OpenReadStream().CopyTo(ms);
                    ms.Position = 0;
                    var fileName = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                                            .FileName
                                            .Trim('"');

                    documents.Add(new DocumentDetails() { FileName = fileName, Content = ms });
                }

                return await ExecuteAsync(async () =>
                {
                    await Service.SubmitDocumentWithExplanation(entityType, entityId, requestId, documents, metadataJson, explanation);
                    return NoContent();
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }    
        }

        /// <summary>
        /// Get documents with specific entity type, entity id and request id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/{requestId}/documents")]

        [ProducesResponseType(typeof(IEnumerable<IDocument>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetDocumentList(string entityType, string entityId, string requestId)
        {
            return await ExecuteAsync(async () =>
            {
                var result = await Service.GetDocumentList(entityType, entityId, requestId);
                return Ok(result);
            });
        }

        /// <summary>
        /// Create a Custom Condition
        /// </summary>
        /// <param name="entityType">entity type</param>
        /// <param name="entityId">entity id</param>
        /// <param name="conditionCategory">category</param>
        /// <param name="description">description</param>
        /// <param name="title">title</param>
        /// <param name="metadata">metadata</param>
        /// <param name="files">documents</param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/custom-condition/")]
        [ProducesResponseType(typeof(ICustomCondition), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddCustom(string entityType, string entityId, string conditionCategory, string description, string title, string metadata, IFormFile[] files)
        {
            try
            {
                IList<DocumentDetails> documents = new List<DocumentDetails>();
                foreach (var file in files)
                {
                    MemoryStream ms = new MemoryStream();
                    file.OpenReadStream().CopyTo(ms);
                    ms.Position = 0;
                    var fileName = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                                            .FileName
                                            .Trim('"');

                    documents.Add(new DocumentDetails() { FileName = fileName, Content = ms });
                }

                var customConditionRequest = new CustomRequestPayload()
                {
                    ConditionCategory = conditionCategory,
                    Title = title,
                    Description = description,
                    MetaData = metadata,
                    Documents = documents
                };

                return await ExecuteAsync(async () =>
                {
                    var customCondition = await Service.AddCustom(entityType, entityId, customConditionRequest);
                    return Ok(customCondition);
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// Delete custom condition with specific entity type, entity id and request id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [HttpDelete("/{entityType}/{entityId}/{requestId}/custom-condition")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteCustom(string entityType, string entityId, string requestId)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await Service.DeleteCustom(entityType, entityId, requestId); return NoContent();
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// Mark as skipped for requested condition.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [HttpPut("/{entityType}/{entityId}/{requestId}/mark-as-skipped")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> MarkAsSkipped(string entityType, string entityId, string requestId)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await Service.MarkAsSkipped(entityType, entityId, requestId); return NoContent();
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }          
        }

        /// <summary>
        /// Delete requested condition with specific entity type, entity id and request id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [HttpDelete("/{entityType}/{entityId}/{requestId}/")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult Delete(string entityType, string entityId, string requestId)
        {
            try
            {
                return Execute(() => { Service.Delete(entityType, entityId, requestId); return NoContent(); });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }


        /// <summary>
        /// Get conditions with specific entity type and entity id for logged in user.
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        /// <param name="entityId">Entity id</param>
        /// <returns></returns>
        //Get Conditions for entityId
        [HttpGet("/{entityType}/{entityId}/mine")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseAssignedRequests(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                var requests = await Service.GetRequestsByUser(entityType, entityId);
                return Ok(requests);
            });         
        }

        /// <summary>
        /// Get request list with specific status for logged in user.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        //Get Conditions for entityId by status
        [HttpGet("/{entityType}/{entityId}/mine/status/{status}")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseAssignedRequestsByStatus(string entityType, string entityId, string status)
        {
            //return Execute(() => Ok(await Service.GetRequestsByUserByStatus(entityType, entityId, status)));

            return await ExecuteAsync(async () =>
            {
                var requests = await Service.GetRequestsByUserByStatus(entityType, entityId, status);
                return Ok(requests);
            });      
        }

        /// <summary>
        /// Get conditions with specific entity type and entity id for logged in user.
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        /// <param name="entityId">Entity id</param>
        /// <returns></returns>
        //Get Conditions for entityId
        [HttpGet("/{entityType}/{entityId}/mine-with-spouse")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRequestsByUserWithSpouse(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                var requests = await Service.GetRequestsByUserWithSpouse(entityType, entityId);
                return Ok(requests);
            });         
        }

        /// <summary>
        /// Get conditions with specific entity type and entity id for logged in user.
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        /// <param name="entityId">Entity id</param>
        /// <param name="status"></param>
        /// <returns></returns>
        //Get Conditions for entityId
        [HttpGet("/{entityType}/{entityId}/mine-with-spouse/status/{status}")]
        [ProducesResponseType(typeof(IEnumerable<IRequest>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRequestsByUserWithSpouseByStatus(string entityType, string entityId, string status)
        {
            return await ExecuteAsync(async () =>
            {
                var requests = await Service.GetRequestsByUserWithSpouseByStatus(entityType, entityId,status);
                return Ok(requests);
            });         
        }
        /// <summary>
        ///  Create new condition with default condition.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="inviteId"></param>
        /// <returns></returns>
        //Create Default Condition
        [HttpPut("/{entityType}/{entityId}/update-recipients-from/{inviteId}/to-username")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateInviteIdWithUserName(string entityType, string entityId, string inviteId)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await Service.UpdateInviteIdWithUserName(entityType, entityId, inviteId);
                    return NoContent();
                });
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }
    }
}